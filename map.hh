#ifndef MAP_H
#define MAP_H

#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "object.hh"
#include "terrain.hh"



class Map
{
    public:
        Map() {};
        Map(std::string filename);
        ~Map();
		Map(const Map&) = delete;
		Map& operator=(const Map&) = delete;
		
        const std::vector<std::vector<Terrain*>>& getObjMap() const;
		const std::vector<std::vector<std::string>>& getEnemyWaves() const;
		const sf::Vector2i& getStart() const;
        void loadMap(std::string filename);
		void drawMap(sf::RenderTarget& rt) const;
		void clearMap();
		const std::string& getMapName() const; //UUSI 6.12. -Lauri

    private:
        std::vector<std::vector<Terrain*>> objMap;
		std::vector<std::vector<std::string>> enemyWaves;
		sf::Vector2i start;
		std::string mapName; //UUSI 6.12. -Lauri


};

//std::ostream& operator<<(std::ostream& os, const Map& map);

#endif // MAP_H
