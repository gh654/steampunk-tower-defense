#ifndef GAME_HH
#define GAME_HH

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <vector>
#include <list>

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cctype>
#include <cstdlib>

#include "namespace.hh"
#include "eventTracker.hh"
#include "objectContainer.hh"
#include "map.hh"
#include "enemy.hh"
#include "tower.hh"
#include "terrain.hh"
#include "projectile.hh"
#include "mapInventory.hh"
#include "player.hh"
#include "highscore.hh"
#include "effects.hh"


class Game
{
public:

	Game();

	~Game();
	void resetGame();  //UUSI 28.11. -Lauri
	void start(); // Alkuscreeni // Markus 20.11.
	bool play();

	void scores();
	void mainMenu();

	void drawPlayerStats();
	void gameOver(sf::RenderTarget& rt);

	void createWaves();
	void newWave(sf::RenderTarget& rt);
    //void shoot(sf::Vector2f enemypos, sf::Vector2f towerpos); // Ei tarvi enää
	//void drawTowerInfo(sf::Vector2i mousePos, int towerType); // Funktiota ei käytetä ccssä? /Markus


private:

	sf::RenderWindow window;
	sf::Clock clock ;
	sf::Time spawnTime;
	sf::Font font ;
	EventTracker events;
	MapInventory inventory;
	Player player;
	Highscore hscore; //Highscore lisätty 25.11. -Lauri
	bool gameEnded;
	bool musicMode;
	sf::Texture bgTex;
	sf::Sprite bg; // cuuli backgroundi taustalle
	float difficultyMultiplier;
	
	bool bombExplosion;

    sf::Music menuMusic;
    sf::Music gameMusic;
    sf::SoundBuffer expSndBuff;
    sf::SoundBuffer bombSndBuff;
    sf::SoundBuffer warSndBuff;
    sf::SoundBuffer bassBombSndBuff;
    sf::Sound bombSound;
    sf::Sound explosionSound;
    sf::Sound warSound;
	sf::Sound bassBombSound; // Ison pommin äänet myös eventTrackerissa
    std::vector<sf::Sound> MGsounds;
	EnemyContainer* enemyWave;
	EnemyContainer activeEnemies;
	std::vector<EnemyContainer*> enemyWaves;
	TowerContainer towers;
	ProjectileContainer projectiles;
	Map map;
	Effects effects;
	std::vector<Explosion*> explosions;
	
};



#endif
