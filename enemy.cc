
#include"enemy.hh"



Enemy::Enemy(EnemyType type, int X = 0, int Y = 0, float mult = 1) : Object(WEnemy,HEnemy),type(type), startPosX(X), startPosY(Y), multiplier(mult), animationClk(new sf::Clock)
{
  //v == { X speed, Y speed}
  v = {0 , 0}; // Muutettu 13.11. - Lauri
  currPos.x = startPosX;
  currPos.y = startPosY;
  updatePosPixel(sf::Vector2f(64*startPosX, 64*startPosY));
  HP = 60;


    switch (type){
      case Ground:

	Points = 50 * multiplier;
	Damage = 20 * multiplier;
	Gold = 70 / multiplier;
	ESpeed = GroundESpeed * multiplier;
	defense = 1.2*multiplier;
	break;
      case Fly:

	Points = 75 * multiplier;
	Damage = 25 * multiplier;
	Gold = 80 / multiplier;
	defense = 1.4*multiplier;
	ESpeed = GroundESpeed * multiplier;
	
	break;
      case Ground_Boss:

	Points = 300 * multiplier;
	Damage = 60 * multiplier;
	Gold = 300 / multiplier;
	defense = 3.8*multiplier;
	ESpeed = GroundBossESpeed * multiplier;
	
	break;
      case Fly_Boss:

	Points = 400 * multiplier;
	Damage = 65 * multiplier;
	Gold = 350 / multiplier;
	defense = 4.0*multiplier;
	ESpeed = GroundBossESpeed * multiplier;
	
	break;

    }
  
  if (ESpeed > GroundESpeed) ESpeed = GroundESpeed;

  direction = down; // /Markus 19.11.
  oldDirection = down;
  anim = 0; //Markus 19.11.
}

// Päivitetty 13.11. - Lauri
void Enemy::update(float dt, std::vector<std::vector<Terrain*>> const &temp_map)
{
    if(v.x == 0 && v.y == 0){
        checkPath(currPos,temp_map);
    }

	// Spriten animaatio: /Markus 19.11.
	if(animationClk->getElapsedTime().asSeconds() > 0.2 || direction != oldDirection)
	{
		oldDirection = direction;
		animationClk->restart();
		animate();
	}

    sf::Vector2f rectPos = rect.getPosition();
    sf::Vector2f movement = dt*v;

    /*std::cout << "x: " << rectPos.x << " y: " << rectPos.y << std::endl;*/
    if(abs(currPosPixel.x-(rectPos.x+movement.x)) >= 64 || abs(currPosPixel.y-(rectPos.y+movement.y)) >= 64){
        int roundX = lrint(rectPos.x / 64) ;
        int roundY = lrint(rectPos.y / 64) ;

        updatePos(sf::Vector2i(roundX, roundY));

        rect.setPosition(64*getPosX(),64*getPosY());
        HPBar_red.setPosition(64*getPosX(),64*getPosY());
        HPBar_gre.setPosition(64*getPosX(),64*getPosY());

        updatePosPixel(rect.getPosition());

        checkPath(currPos, temp_map);

    }
    else{
        rect.move(movement);
        HPBar_gre.move(movement);
        HPBar_red.move(movement);
    }
}


void Enemy::animate()
{
	if (anim == 0 || anim == 1) // 1. frame. anim 0: tekstuuria ei flipattu, anim 1: tekstuuri flipattu
		{
			switch(direction)
			{
				case down:
					rect.setTextureRect(sf::IntRect(64, 0, 64, 64));
					break;
				case up:
					rect.setTextureRect(sf::IntRect(128, 0, 64, 64));
					break;
				case left:
					rect.setTextureRect(sf::IntRect(64, 64, 64, 64));
					if (anim == 0)
					{
						rect.setScale(-1,1);
						rect.setOrigin(64,0);
						anim = 1;
					}
					break;
				case right:
					rect.setTextureRect(sf::IntRect(64, 64, 64, 64));
					if (anim == 1)
					{
						rect.setScale(1,1);
						rect.setOrigin(0,0);
						anim = 0;
					}
					break;
			}
			if (anim == 0) anim = 3;
			if (anim == 1) anim = 4;
		}
		else if (anim == 3 || anim == 4){ // Toinen frame. anim 3: tekstuuria ei flipattu, anim 4: tekstuuri flipattu
			switch(direction)
			{
				case dir::down:
					rect.setTextureRect(sf::IntRect(0, 0, 64, 64));
					break;
				case dir::up:
					rect.setTextureRect(sf::IntRect(128, 64, 64, 64));
					break;
				case dir::left:
					rect.setTextureRect(sf::IntRect(0, 64, 64, 64));
					if (anim == 3)
					{
						rect.setScale(-1,1);
						rect.setOrigin(64,0);
						anim = 4;
					}
					break;
				case dir::right:
					rect.setTextureRect(sf::IntRect(0, 64, 64, 64));
					if (anim == 4)
					{
						rect.setScale(1,1);
						rect.setOrigin(0,0);
						anim = 3;
					}
					break;
			}
			if (anim == 3) anim = 0;
			if (anim == 4) anim = 1;
		}
}


void Enemy::updateTex()
{

  if (get_Type() == Ground)
    tex.loadFromFile("graphics/terminaattori.png");

  if (get_Type() == Fly)
    tex.loadFromFile("graphics/flying.png");
	
	if (get_Type() == Ground_Boss)
		tex.loadFromFile("graphics/ground_boss.png");
		
	if (get_Type() == Fly_Boss)
		tex.loadFromFile("graphics/fly_boss.png");
	
	

  // Oma sprite //Markus 20.11.
	//tex.loadFromFile("graphics/terminaattori.png"); // Oma sprite //Markus 19.11.
  //tex.loadFromFile("graphics/orc.png");
  rect.setPosition(64*startPosX,64*startPosY);
  rect.setTexture(&tex, true);
  rect.setTextureRect(sf::IntRect(0, 0, 64, 64)); //Markus 19.11.
  updatePosPixel(rect.getPosition());

  HPBar_red.setSize(sf::Vector2f(60, 2));
  HPBar_red.setFillColor(sf::Color::Red);
  HPBar_red.setOutlineThickness(1);
  HPBar_red.setPosition(64*startPosX, 64*startPosY);


  HPBar_gre.setSize(sf::Vector2f(HP, 2));
  HPBar_gre.setFillColor(sf::Color::Green);
  HPBar_gre.setOutlineThickness(1);
  HPBar_gre.setPosition(64*startPosX, 64*startPosY);

}

float Enemy::get_HP()const{               //Arber ---------------------------------------------------------------------------
return HP;}

void Enemy::get_damage(int dama){      //Arber ---------------------------------------------------------------------------
HP-=(float)dama / defense;
if(HP<=0)
  HP=0;
HPBar_gre.setSize(sf::Vector2f(HP, 2));
}

sf::Vector2i Enemy::get_Currpos()const{      //Arber ---------------------------------------------------------------------------
return currPos;}



void Enemy::draw_HP(sf::RenderTarget &rt) const   //Arber ---------------------------------------------------------------------------
{
  rt.draw(HPBar_red);
  rt.draw(HPBar_gre);
}


// Päivitetty 6.12. -Lauri
void Enemy::checkPath(sf::Vector2i pos, std::vector<std::vector<Terrain*>> const &coll_map)
{
    int TileY = (pos.y);
    int TileX = (pos.x);

    if(coll_map[TileY][TileX]->getType() == 3)
    {
        v = {0,0} ;
    }
    else
    {
    //Jos vihollinen on aloitusruudussa. Tämä pitää tarkastaa, sillä muuten tulee segfault, jos yritetään
    //tarkastaa ympäröiviä ruutuja, jotka sijaitsevat ikkunan ulkopuolella aka. niitä ei ole olemassa.

        int xDir = 0; // X speed
        int yDir = 0; // Y speed
		std::vector<int> yDirections;
		std::vector<int> xDirections;
		
        if(TileY != 11 && v.y >= 0) // Game area grid max value
        {
            if(coll_map[TileY+1][TileX]->getType()  == 1 || coll_map[TileY+1][TileX]->getType()  == 3)
            {
                yDir = 1;
				yDirections.push_back(1);
            }
        }
        if(TileY != 0 && v.y <= 0)
        {
            if(coll_map[TileY-1][TileX]->getType()  == 1 || coll_map[TileY-1][TileX]->getType()  == 3)
            {
                yDir = -1;
				yDirections.push_back(-1);
            }
        }
        if(TileX != 12 && v.x >= 0) // Game area grid max value
        {
            if(coll_map[TileY][TileX+1]->getType()  == 1 || coll_map[TileY][TileX+1]->getType()  == 3)
            {
                xDir = 1;
				xDirections.push_back(1);
            }
        }
        if(TileX != 0 && v.x <= 0)
        {
            if(coll_map[TileY][TileX-1]->getType()  == 1 || coll_map[TileY][TileX-1]->getType()  == 3)
            {
                xDir = -1;
				xDirections.push_back(-1);
            }
        }
		
		int possibilities = yDirections.size() + xDirections.size();
		if(possibilities > 1)
		{
			int random = rand() % possibilities;
			if(yDirections.size() == 0){
				v.x = xDirections[random]*ESpeed;
				v.y = 0;
			}
			else if(xDirections.size() == 0){
				v.x = 0;
				v.y = yDirections[random]*ESpeed;
			}
			else{
				if(random <= (int)yDirections.size()-1){
					v.x = 0;
					v.y = yDirections[random]*ESpeed;
					
				}
				else{
					v.x = xDirections[random-yDirections.size()]*ESpeed;
					v.y = 0;
				}
			}
			
		}
		else
		{
			v.x = xDir*ESpeed;
			v.y = yDir*ESpeed;
		}
		
		 // Päivittää suunnan spritejä varten  6.12. -Lauri.
		if(v.y > 0)
			direction = down;
		else if(v.y < 0)
			direction = up;
		else if(v.x > 0)
			direction = right;
		else if(v.x < 0)
			direction = left;


    }
}


void Enemy::updatePos(sf::Vector2i pos)
{
  currPos = pos;
}

void Enemy::updatePosPixel(sf::Vector2f pos)
{
  currPosPixel = pos ;
}

int Enemy::getPosPixelX() const
{
	int temp = int(currPosPixel.x + 0.5);
	return temp;
}

int Enemy::getPosPixelY() const
{
	int temp = int(currPosPixel.y + 0.5);
	return temp;
}

int Enemy::getPosX() const
{
  return currPos.x ;
}

int Enemy::getPosY() const
{
  return currPos.y;
}

//arber seuraavat neljä lisätty 21.11
EnemyType Enemy::get_Type()const{
  return type;
}

int Enemy::make_Damage()const{
  return Damage;
}

int Enemy::get_Points()const{
  return Points;
}

int Enemy::get_Gold()const{
  return Gold;
}



