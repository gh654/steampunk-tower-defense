#ifndef PROJECTILE_HH
#define PROJECTILE_HH

#include "object.hh"
#include "enemy.hh"
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>


class Projectile : public Object
{
public:
    Projectile(sf::Vector2f, sf::Vector2f, int type = 1, int power = 1);

    void update(float, sf::Vector2f);

	bool isPast(); // Onko projectile ohittanut kohteensa

	void setPast(); // Modaa projectile tuhottavaksi

    void updateTex();

	int get_type() const;

	bool check_Enemy(Enemy); // Kopsattu towerista

	int getRange();

	int make_Damage()const;

	bool missileTimeout(); // Tarkistaa onko missile ollut elossa tarpeeksi kauan

	sf::Vector2f getTarget() const; // Returns the target of the projectile


private:
	int range; // Missilelle
    int posX;
	int posY;
	sf::Texture tex;
	int sequence; // MG projectile animating
    sf::Vector2f target; // Vihun koordinaatit
	bool pastTarget;
	int type; // Markus 25.11. projectilen tyyppi: 1 - MG, 2 - missile, 3 - Area
	sf::Clock* aliveTime; //Missilelle... Markus 25.11.
	int Power; // Markus 25.11., missilelle, kopsattu towerista

};


#endif
