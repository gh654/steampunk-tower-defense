#include "effects.hh"


Effects::Effects()
{
	explosionTex.loadFromFile("graphics/explosion.png");
}


Effects::~Effects()
{}



Explosion::Explosion(int X, int Y, int type) : posX(X), posY(Y), type(type)
{
	sequenceX = 0 ;
	sequenceY = 0 ;
	blowCounter = 0 ;
	direction = blow_up ;
}


Explosion::Explosion()
{
	sequenceX = 0 ;
	sequenceY = 0 ;
	posX = 0;
	posY = 0;
	blowCounter = 0 ;
	direction = blow_up ;
	type = 3;
}


Explosion::~Explosion()
{}


void Explosion::animate(sf::Texture& tex, sf::RenderTarget& rt)
{
	if (type == 0)
	{
		rect.setSize(sf::Vector2f(32, 32));
		rect.setTexture(&tex, true);
		//rect.setTextureRect(sf::IntRect(64*sequenceX,0 , 64, 64));
		rect.setTextureRect(sf::IntRect(96*sequenceX, 1600, 96, 96));
		rect.setPosition(posX+16,posY+16);
		
		sequenceX++ ;
	
		rt.draw(rect);
	}
	
    else if (type == 1)
	{
	 rect.setSize(sf::Vector2f(64, 64));
	 rect.setTexture(&tex, true);


	 rect.setTextureRect(sf::IntRect(64*sequenceX,0 , 64, 64));
	 rect.setPosition(posX,posY);

	 sequenceX++ ;
	 rt.draw(rect);
	}

	 else if (type == 2)
	{
		rect.setSize(sf::Vector2f(400, 400));
		rect.setTexture(&tex, true);
		rect.setTextureRect(sf::IntRect(256*sequenceX, 64 + 256 * sequenceY, 256, 256));
		rect.setPosition(posX-240,posY-220);

		if(sequenceX == 7){
	 		sequenceX = 0;
	 		sequenceY++;
		}
		else{
			sequenceX++ ;
		}

		rt.draw(rect);
	}
}



bool Explosion::animateBig(sf::Texture& tex, sf::RenderTarget& rt)
{
	rect.setSize(sf::Vector2f(512, 512));
	rect.setTexture(&tex, true);
	rect.setTextureRect(sf::IntRect(256*sequenceX, 64 + 256 * sequenceY, 256, 256));
	rect.setPosition(posX,posY);

	if(sequenceX == 7){
	 		sequenceX = 0;
	 		sequenceY++;
	}
	else{
	 sequenceX++ ;
	}

	if(sequenceY  == 5){
		sequenceY = 0 ;
		sequenceX = 0 ;
		blowCounter ++ ;

		switch(direction){

			case blow_up:
				posX = posX + 100;
				direction = blow_right ;

				break;

			case blow_right:
				posY = posY + 100;
				direction = blow_down ;

				break;

			case blow_down:
				posX = posX - 100;
				direction = blow_left;

				break;

			case blow_left:
				posY = posY - 100;
				direction = blow_up ;
				break;
		}
	}

	rt.draw(rect);
	if(blowCounter < 3){
		return true ;}

	else{
		return false ;}
}


int Explosion::getSequenceX() const
{
	return sequenceX;
}

int Explosion::getSequenceY() const
{
	return sequenceY;
}

int Explosion::getType() const
{
	return type;
}

void Explosion::giveCoordinates(sf::Vector2i& coords)
{
	posX = coords.x - 256 ;
	posY = coords.y - 256 ;

}

void Explosion::setSequence(int x, int y)
{
	sequenceX = x ;
	sequenceY = y ;
}


void Explosion::resetCounter()
{
	blowCounter = 0 ;
}
