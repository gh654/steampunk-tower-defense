#export LD_LIBRARY_PATH=~/SFML-2.1/lib && ./td
SFML=~/SFML-2.1
CC=g++-4.6
CFLAGS=-c -g -std=c++0x -Wall -Wextra -pedantic -I $(SFML)/include
LDFLAGS=-lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -L $(SFML)/lib -Wl,-rpath=$(SFML)/lib -Wl,-rpath-link=$(SFML)/lib
SOURCES=main.cc map.cc enemy.cc tower.cc projectile.cc terrain.cc objectContainer.cc mapInventory.cc eventTracker.cc player.cc game.cc effects.cc highscore.cc
OBJECTS=main.o map.o enemy.o tower.o projectile.o terrain.o objectContainer.o mapInventory.o eventTracker.o player.o game.o effects.o highscore.o
EXECUTABLE=td

all: $(SOURCES) $(EXECUTABLE)
  
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@ 

.cc.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f $(OBJECTS) $(EXECUTABLE)
