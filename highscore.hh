#ifndef HIGHSCORE_HH
#define HIGHSCORE_HH

#include <string>
#include <list>
#include <cstdlib>

class Highscore{
public:
	Highscore(){}
	~Highscore(){}
	Highscore(std::string fname);

	const std::list<std::pair<std::string,int>>& getScores() const;
	void addScore(std::string name, int score);
	void loadScores(std::string fname);
	void saveScores(std::string fname);
	void sort();
	void clearScores(); //UUSI 6.12. -Lauri

private:
	std::list<std::pair<std::string,int>> scores;

};

bool compare_scores(std::pair<std::string,int> first, std::pair<std::string,int> second);
std::ostream& operator<<(std::ostream& os, const Highscore& hscore);

#endif
