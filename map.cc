
#include "map.hh"
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>


    Map::Map(std::string filename){
        loadMap(filename);
    }


    Map::~Map(){
        for(size_t i = 0; i < objMap.size(); i++){
            for(size_t j = 0; j < objMap[i].size(); j++){
                delete objMap[i][j];
            }
        }
    }

    const std::vector<std::vector<Terrain*>>& Map::getObjMap() const{
        return objMap;
    }
	
	const std::vector<std::vector<std::string>>& Map::getEnemyWaves() const{
		return enemyWaves;
	}

	const sf::Vector2i& Map::getStart() const{
		return start;
	}

    void Map::loadMap(std::string filename){
        std::ifstream openFile(filename);

        if(openFile.is_open()){
			bool readMap = false;
			bool readWaves = false;
			int xcoord = 0;
			int ycoord = 0;
            while(!openFile.eof()){
                std::string str, value;
                std::getline(openFile, str);

				//std::cout << str << std::endl;
			
				if(str.find("-MapEnd") != std::string::npos)
					readMap = false;
				else if(str.find("-WavesEnd") != std::string::npos)
					readWaves = false;
					
				
				std::stringstream ss(str);
                std::vector<Terrain*> mapRow;
				std::vector<std::string> enemyWave;
				
				
				//GRASS = 0, PATH = 1, START=2, FINISH=3, TREE=4
				if(readMap){
					while(std::getline(ss, value, ' ')){
						if(value.find("G") != std::string::npos)
							mapRow.push_back(new Terrain(0,xcoord++,ycoord));
						else if(value.find("P") != std::string::npos)
							mapRow.push_back(new Terrain(1,xcoord++,ycoord));
						else if(value.find("S") != std::string::npos){
							mapRow.push_back(new Terrain(2,xcoord++,ycoord));
							start = sf::Vector2i(xcoord-1,ycoord);
							}
						else if(value.find("F") != std::string::npos){
							mapRow.push_back(new Terrain(3,xcoord++,ycoord));
						}
						else if(value.find("T") != std::string::npos){
							mapRow.push_back(new Terrain(4,xcoord++,ycoord));
							}
						else
							throw std::invalid_argument("map error");
					}
					if(mapRow.size() > 0){
						objMap.push_back(mapRow);
						mapRow.clear();
						xcoord = 0;
						ycoord++;
					}
				}
				if(readWaves){
					while(std::getline(ss, value, ' ')){
						if(value.find("g") != std::string::npos)
							enemyWave.push_back("g");
						else if(value.find("f") != std::string::npos)
							enemyWave.push_back("f");

						else if(value.find("G") != std::string::npos)
							enemyWave.push_back("G");
						else if(value.find("F") != std::string::npos)
							enemyWave.push_back("F");
						else
							throw std::invalid_argument("Unknown enemy.");
							
						/*else if(value.find("b") != std::string::npos)
							enemyWave.push_back("b");*/
					}
					if(enemyWave.size() > 0){
						enemyWaves.push_back(enemyWave);
						enemyWave.clear();
					}
				}
				if (str.find("-MapBegin") != std::string::npos)
					readMap = true;
				else if(str.find("-WavesBegin") != std::string::npos)
					readWaves = true;
			
			}
			mapName = filename; //UUSI 6.12. -Lauri
		}
		// 1 = sivuttain, 2 = ylös-alas, 3 = vasen alas, 4 = oikea alas, 5 = vasen ylös, 6 = oikea ylös
		for(size_t i = 0; i < objMap.size(); i++){
            for(size_t j = 0; j < objMap[i].size(); j++){
            	if(objMap[i][j]->getType() == 1 )
            	{
            		if(objMap[i+1][j]->getType() == 1 && objMap[i-1][j]->getType() == 1){ // ylä ja alapuolella polku
            			objMap[i][j]->adjustPaths(2);}
            		
            		else if(objMap[i][j+1]->getType() == 1 && objMap[i][j-1]->getType() == 1){ // oikealla ja vasemmalla polku
            			objMap[i][j]->adjustPaths(1);}
            			
            		else if(objMap[i-1][j]->getType() == 1 && objMap[i][j+1]->getType() == 1){ // ylhäällä ja oikealla polku
            			objMap[i][j]->adjustPaths(6);}

            		else if(objMap[i-1][j]->getType() == 1 && objMap[i][j-1]->getType() == 1){ // ylhäällä ja vasemmalla polku
            			objMap[i][j]->adjustPaths(5);}

            		else if(objMap[i+1][j]->getType() == 1 && objMap[i][j+1]->getType() == 1){ // alhaalla ja oikealla polku
            			objMap[i][j]->adjustPaths(4);}
            			
            		else if(objMap[i+1][j]->getType() == 1 && objMap[i][j-1]->getType() == 1){ // alhaalla ja vasemmalla polku
            			objMap[i][j]->adjustPaths(3);}
            			
            			
            		else if(objMap[i+1][j]->getType() == 2 || objMap[i-1][j]->getType() == 2 || objMap[i+1][j]->getType() == 3 || objMap[i-1][j]->getType() == 3){ // alhaalla ja vasemmalla polku
            			objMap[i][j]->adjustPaths(2);}
            		
            		else if(objMap[i][j+1]->getType() == 2 || objMap[i][j-1]->getType() == 2 || objMap[i][j-1]->getType() == 3 || objMap[i][j+1]->getType() == 3){ // alhaalla ja vasemmalla polku
            			objMap[i][j]->adjustPaths(1);}
            		
            		else{		  //bugien varalta piirtää joka tapauksessa jotain
            		    objMap[i][j]->adjustPaths(2);}
            		
            	}
            }
         }	
            		
		
	}

// Muutettu 25.11. -Lauri
	void Map::drawMap(sf::RenderTarget& rt) const{
		for(size_t i = 0; i < objMap.size(); i++){
            for(size_t j = 0; j < objMap[i].size(); j++){
                objMap[i][j]->draw(rt);
				//objMap[i][j]->updateTex();
            }
        }
	}
	
	void Map::clearMap()
	{
		for(size_t i = 0; i < objMap.size(); i++){
			for(size_t j = 0; j < objMap[i].size(); j++){
				delete objMap[i][j];
			}
		}
		
		objMap.clear();
		enemyWaves.clear();
		
	}
	
	//UUSI 6.12. -Lauri
	const std::string& Map::getMapName() const{
		return mapName;
	}
	
	

    /*std::ostream& operator<<(std::ostream& os, const Map& map){
		os << "Map:" << std::endl;
		
		auto objects = map.getObjMap();

        for(size_t i = 0; i < objects.size(); i++){
            for(size_t j = 0; j < objects[i].size(); j++){
                os << objects[i][j]->getName() << " ";
            }
            os << std::endl;
        }
		
		os << "Enemies:" << std::endl;
		
		auto enemies = map.getEnemyWaves();
		
		for(size_t i = 0; i < enemies.size(); i++){
            for(size_t j = 0; j < enemies[i].size(); j++){
                os << enemies[i][j]->getName() << " ";
            }
            os << std::endl;
        }
		
        return os;
    }*/
