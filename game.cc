//export LD_LIBRARY_PATH=~/Desktop/C++_koodit/SFML/lib && ./main

#include "game.hh"





Game::Game() : window(sf::VideoMode(WWidth, WHeight), "Steampunk Tower Defense", sf::Style::Close)
{
		font.loadFromFile("steampunk.ttf");
		window.setFramerateLimit(60);
		bgTex.loadFromFile("graphics/background.png");
        bg.setTexture(bgTex);

		expSndBuff.loadFromFile("audio/explosion-02.ogg");
		bombSndBuff.loadFromFile("audio/bomb-02.ogg");
		warSndBuff.loadFromFile("audio/gun_battle_sound.wav");
		bassBombSndBuff.loadFromFile("audio/Explosion_Ultra_Bass.wav");
		explosionSound.setBuffer(expSndBuff);
		explosionSound.setVolume(40);
        bombSound.setBuffer(bombSndBuff);
        bombSound.setVolume(70);
        warSound.setBuffer(warSndBuff);
        warSound.setVolume(30);
        warSound.setLoop(true);
        bassBombSound.setBuffer(bassBombSndBuff);
		bombExplosion = false;

        menuMusic.openFromFile("audio/Machines.ogg");
        gameMusic.openFromFile("audio/Machine March.ogg");
        menuMusic.setLoop(true);
        gameMusic.setLoop(true);
        menuMusic.setVolume(50);
        gameMusic.setVolume(50);
        musicMode = true;
}



Game::~Game()
{}


 //UUSI 28.11. -Lauri
void Game::resetGame()
{
  map.clearMap();

  if(enemyWave != NULL)
	enemyWave->clearContainer();

  activeEnemies.clearContainer();

  for(size_t i = 0; i < enemyWaves.size(); i++)
  {
	  delete enemyWaves[i];
  }
  enemyWaves.clear();

  towers.clearContainer();

  projectiles.clearContainer();

  for(size_t i = 0; i < explosions.size(); i++)
  {
	  delete explosions[i];
  }
  explosions.clear();

  player.reset();


}

void Game::start() // Alkuscreeni // Markus 20.11.
{
	bool exitStatus = false;

	sf::String userInput; // Käyttäjän teksti
	sf::String info; // Ohjetekstit
	int phase = 0; // Pitää kirjaa aloitusruudun vaiheista: Pelaajan nimen syöttö, mapin syöttö, kullan määrä
	int fileError = 0 ;
	int chosenDifficulty = 0;

	std::string mapname = "map1.txt";
	std::string playername = "";
	int selectedMap = 1;
	int gold;

	sf::RectangleShape confirmBox;
	sf::Texture confirm;

 	confirm.loadFromFile("graphics/confirm.png");
 	confirmBox.setSize(sf::Vector2f(60,30));
 	confirmBox.setPosition(WWidth/2 - 60, WHeight/2 + 80);
 	confirmBox.setTexture(&confirm, true);

	sf::Text diffEasyText("Easy", font, 30);
	sf::Text diffNormalText("Normal", font, 30);
	sf::Text diffHardText("Hard", font, 30);

	userInput = playername; // Alustetaan

	std::string debug; // Debuggaamiseen
	sf::Event event;  //UUSI 28.11. -Lauri

	while (window.pollEvent(event)) //UUSI 28.11. -Lauri
        {}

	while(window.isOpen() && phase != 3)
    {
		window.draw(bg);
		//debug = userInput;
		//std::cout << debug << std::endl; // Debug

		//UUSI 28.11. -Lauri
		sf::Text newGameText("New game", font, 80);
		newGameText.setPosition(WWidth/2-140,100);
		window.draw(newGameText);

		//UUSI 28.11. -Lauri
		sf::Text playerText("Player", font, 20);
		playerText.setPosition(400, 420);

		switch (phase)
		{
			case 2:
				info = "Choose your difficulty:";
				diffEasyText.setPosition(400, 420);
				diffNormalText.setPosition(485, 420);
				diffHardText.setPosition(600, 420);
				window.draw(diffEasyText);
				window.draw(diffNormalText);
				window.draw(diffHardText);
				break;

			case 0:
				window.draw(playerText); //UUSI 28.11. -Lauri
				info = "Input player name and click OK";
				break;
			case 1:
				info = "Use Up and Down keys or input map name\nand click OK";
				break;
			default:
				info = "";
				break;
		}

		sf::Text phaseText(info, font, 20);
		phaseText.setPosition(370,370);
		window.draw(phaseText);

		if (phase == 2)
		{

			sf::Vector2i mousePos = sf::Mouse::getPosition(window);
			sf::Vector2f mPos(mousePos.x,mousePos.y);

			if(diffEasyText.getGlobalBounds().contains(mPos))
			{
				diffEasyText.setColor(sf::Color(150,180,240,255));
				if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
					chosenDifficulty = 1;
				}
			}
			else diffEasyText.setColor(sf::Color::White);

			if(diffNormalText.getGlobalBounds().contains(mPos))
			{
				diffNormalText.setColor(sf::Color(150,180,240,255));
				if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
					chosenDifficulty = 2;
				}
			}
			else diffNormalText.setColor(sf::Color::White);

			if(diffHardText.getGlobalBounds().contains(mPos))
			{
				diffHardText.setColor(sf::Color(150,180,240,255));
				if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
					chosenDifficulty = 3;
				}
			}
			else diffHardText.setColor(sf::Color::White);

			if (chosenDifficulty)
			{
				switch (chosenDifficulty)
				{
					case 1:
						difficultyMultiplier = DMultEasy;
						gold = EasyGold;
						break;
					case 2:
						difficultyMultiplier = DMultNormal;
						gold = NormalGold;
						break;
					case 3:
						difficultyMultiplier = DMultHard;
						gold = HardGold;
						break;
				}
				//goldamount = userInput;
				phase++;
				menuMusic.stop();
				sf::Text loadmsg("Loading... please wait.", font, 40);
				loadmsg.setPosition(WWidth/2 + 100, WHeight/2 + 80);
				window.draw(loadmsg);
			}


		}

		while (window.pollEvent(event))
        {
			if(event.type == sf::Event::Closed)
			{
				window.close();
				exitStatus = true;
			}

			if (phase == 2) break;

			if(event.type == sf::Event::TextEntered && event.text.unicode != '\b' && event.text.unicode != '\r') // Tarkistaa myös ettei teksti ole backspace tai enter
			{

				// alternate -versio
				// userInput.insert(userInput.getSize(), event.text.unicode);
				if(event.key.code != sf::Keyboard::Return && !sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) //UUSI 28.11. -Lauri
				{
				userInput += event.text.unicode;
				}
			}

			if(event.type == sf::Event::KeyPressed || event.type == sf::Event::MouseButtonPressed)
			{

				if(event.key.code == sf::Keyboard::BackSpace && userInput.getSize()) // delete the last character
				{
					// sf::String -versio
					userInput.erase(userInput.getSize() - 1);
					/*
					std::string tmp;
					tmp = userInput;
					userInput.clear();
					userInput = tmp;
					*/
				}

				else if ((event.key.code == sf::Keyboard::Up || event.key.code == sf::Keyboard::Down) && phase == 1)
				{
					if (event.key.code == sf::Keyboard::Up)
						selectedMap++;
					else if (event.key.code == sf::Keyboard::Down)
						selectedMap--;

					std::stringstream ss;
					ss << "map" << selectedMap << ".txt";
					mapname = ss.str();
					userInput = mapname;
				}

				else if ((event.mouseButton.button == sf::Mouse::Left || event.key.code == sf::Keyboard::Return))
			 	{
				//sf::Vector2i mousePos = sf::Mouse::getPosition(window);
				sf::Vector2i mousePos = sf::Mouse::getPosition(window);
					if(((mousePos.x > WWidth/2 - 60 && mousePos.x <  WWidth/2 + 60) && (mousePos.y > WHeight/2 + 80 && mousePos.y < WHeight/2 + 80 + 30)) || event.key.code == sf::Keyboard::Return)
					{
						if (phase == 0)
						{

							playername = userInput;
							userInput = mapname;
							phase++;

						}
						else if (phase == 1)
						{
							mapname = userInput;
							std::stringstream pathSS;
							pathSS << "maps/";
							pathSS << mapname;
							mapname = pathSS.str();
							std::ifstream testfile(mapname);

							if(!testfile)
							{
								fileError = 1 ;
							}

							else{
								fileError = 0 ;
								//userInput = goldamount;
								phase++;
							}
						}

					}

				}

			}

		}

		if (phase < 2)
		{
			sf::Text input(userInput, font, 20);
			input.setPosition(470, 420);
			window.draw(input);


			sf::Vector2i mousePos = sf::Mouse::getPosition(window);

			if((mousePos.x > WWidth/2 - 60 && mousePos.x <  WWidth/2 + 60) && (mousePos.y > WHeight/2 + 80 && mousePos.y < WHeight/2 + 80 + 30))
			{
				confirmBox.setTextureRect(sf::IntRect(60, 0, 60, 30));
				window.draw(confirmBox);
			}

			else
			{
				confirmBox.setTextureRect(sf::IntRect(0, 0, 60, 30));
				window.draw(confirmBox);
			}

			if(fileError == 1) {
				sf::Text errormsg("Error! Check your filename!", font, 40);
				errormsg.setPosition(WWidth/2 + 100, WHeight/2 + 80);
				errormsg.setColor(sf::Color::Red);
				window.draw(errormsg);
			}
		}


		window.display();
	}

	if (!exitStatus)
	{

		//gold = std::strtol(goldamount.c_str(), NULL, 10);


		map.loadMap(mapname);
		createWaves() ;

		player.setName(playername);
		player.addGold(gold);

		//UUSI 28.11. -Lauri
		if(this->play())
		  this->scores();
		resetGame();
	}
}

 //UUSI 28.11. -Lauri
void Game::mainMenu()
{
    if (menuMusic.getStatus() != sf::SoundSource::Playing && musicMode)
        menuMusic.play();

	sf::Text newGameText("New game", font, 80);
	sf::Text highscoresText("Highscores", font, 80);
	sf::Text musicText("Music: on", font, 80);
	sf::Text exitText("Exit", font, 80);

	newGameText.setPosition(WWidth/2-140,100);
	highscoresText.setPosition(WWidth/2-140,200);
	musicText.setPosition(WWidth/2-140,300);
	exitText.setPosition(WWidth/2-140,400);

	//hscore.loadScores("scores.txt");

	while(window.isOpen())
	{
		window.clear();
		window.draw(bg);
		window.draw(newGameText);
		window.draw(highscoresText);
		window.draw(musicText);
		window.draw(exitText);
		window.display();
		sf::Vector2i mousePos = sf::Mouse::getPosition(window);
		sf::Vector2f mPos(mousePos.x,mousePos.y);

		sf::Event event;


		if(newGameText.getGlobalBounds().contains(mPos))
		{
			newGameText.setColor(sf::Color(150,180,240,255));
			if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
				this->start();
			}
		}
		else newGameText.setColor(sf::Color::White);

		if(highscoresText.getGlobalBounds().contains(mPos))
		{
			highscoresText.setColor(sf::Color(150,180,240,255));
			if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
				this->scores();
			}
		}
		else highscoresText.setColor(sf::Color::White);

        while (window.pollEvent(event))
        {
			if(event.type == sf::Event::Closed)
			{
				window.close();
			}

            if (event.type == sf::Event::MouseButtonReleased && musicText.getGlobalBounds().contains(mPos))
            {
                if (musicMode == true)
                {
                    musicMode = false;
                    musicText.setString("Music: off");
                    menuMusic.stop();
                }
                else
                {
                    musicMode = true;
                    musicText.setString("Music: on");
                    menuMusic.play();
                }
            }
		}
		if (musicText.getGlobalBounds().contains(mPos))
        {
            musicText.setColor(sf::Color(150,180,240,255));
        }
        else
            musicText.setColor(sf::Color::White);

		if(exitText.getGlobalBounds().contains(mPos))
		{
			exitText.setColor(sf::Color(150,180,240,255));
			if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
				window.close();
			}
		}
		else exitText.setColor(sf::Color::White);

	}
}
	//return false;



//UUSITTU 6.12. -Lauri
void Game::scores()
{
	if(map.getMapName().size() == 0)
		hscore.loadScores("maps/map1.txt");
	/*else
		hscore.loadScores(map.getMapName());
		*/
    if (menuMusic.getStatus() != sf::SoundSource::Playing && musicMode)
        menuMusic.play();
	sf::RectangleShape re;
	re.setFillColor(sf::Color::Black);
	re.setSize(sf::Vector2f(WWidth,WHeight));
	re.setPosition(0,0);

	std::stringstream ss;
	ss << hscore;
	sf::Text scoreText(ss.str(), font, 40);
	sf::Text titleText("Highscores", font, 80);
	sf::Text infoText("Press Escape to exit", font, 40);

	titleText.setPosition(10,10);
	infoText.setPosition(10,100);
	scoreText.setPosition(WWidth/2 - 100,WHeight);


	int movement = 0;
	int step = 2;
	bool toMainMenu = false;  //UUSI 28.11. -Lauri



	sf::Event event;
	std::string mapname = "map1.txt";
	sf::String userInput = mapname;
	std::string mapPath;
	int selectedMap = 1;
	sf::Text inputInfo("Use Up/Down or\nenter filename and\npress Enter.", font, 30);
	inputInfo.setPosition(10, 200);
	sf::Text input(userInput, font, 30);
	input.setPosition(10, 320);

	while (window.pollEvent(event)) //UUSI 28.11. -Lauri
	{}

	while(window.isOpen() && !toMainMenu){

		window.draw(re);
		window.draw(titleText);
		window.draw(infoText);
		window.draw(inputInfo);
		window.draw(input);

		scoreText.move(0,-step);
		movement += step;
		window.draw(scoreText);

		window.display();

		int listSize = hscore.getScores().size();
		if(movement  > listSize*47 + WHeight){
			movement = 0;
			scoreText.setPosition(WWidth/2 - 100,WHeight);
		}


		while (window.pollEvent(event))
        {
			if(event.type == sf::Event::Closed)
			{
				window.close();
			}

			if(event.key.code == sf::Keyboard::Escape)
			{
				toMainMenu = true;
    		}
			else if (event.type == sf::Event::KeyPressed && (event.key.code == sf::Keyboard::Up || event.key.code == sf::Keyboard::Down))
			{
				if (event.key.code == sf::Keyboard::Up)
						selectedMap++;
				else if (event.key.code == sf::Keyboard::Down)
						selectedMap--;

				std::stringstream ss;
				ss << "map" << selectedMap << ".txt";
				mapname = ss.str();
				userInput = mapname;
				input.setString(userInput);
			}
			else
			{
				if(event.type == sf::Event::TextEntered && event.text.unicode != '\b' && event.text.unicode != '\r') // Tarkistaa myös ettei teksti ole backspace tai enter
				{

					if(event.key.code != sf::Keyboard::Return && !sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) //UUSI 28.11. -Lauri
					{
						userInput += event.text.unicode;
						input.setString(userInput);
					}
				}

				if(event.type == sf::Event::KeyPressed)
				{
					if(event.key.code == sf::Keyboard::BackSpace && userInput.getSize()) // delete the last character
					{

						userInput.erase(userInput.getSize() - 1);
						input.setString(userInput);

					}
					if (event.key.code == sf::Keyboard::Return)
					{
						std::stringstream pathSS;
						pathSS << "maps/";
						pathSS << userInput.toAnsiString();
						std::ifstream testfile(pathSS.str());

						if(!testfile)
						{
							scoreText.setString("Incorrect map name.");
						}
						else
						{
							hscore.clearScores();
							hscore.loadScores(pathSS.str());
							std::stringstream ss2;
							ss2 << hscore;
							scoreText.setString(ss2.str());
						}
					}
				}
			}
		}
	}
}

bool Game::play()
{
    if (musicMode)
        gameMusic.play();

	bool pauseBetweenWaves = true;
	gameEnded = false;

	enemyWave = NULL;
	/*enemyWave = enemyWaves.back(); // Jotain pitää tehdä tälle, jotta alussa on 10 sekunnin aika
	enemyWaves.pop_back();*/

	clock.restart();
	spawnTime = sf::seconds(0);


    while(window.isOpen())
    {
		sf::Time dt;
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))  //UUSI 28.11. -Lauri
			{
				dt = clock.restart();
				sf::Text infoText("Press F1 to exit or F2 to continue.", font, 40);

				infoText.setPosition(WWidth/2 - 557/2,WHeight/2 -150);
				window.draw(infoText);
				window.display();
				bool toMainMenu = false;

				gameMusic.pause();
				if (!MGsounds.empty()) // Lopeta äänentoisto
				{
					warSound.stop();
					MGsounds.pop_back();
				}

				while(true){
					if(sf::Keyboard::isKeyPressed(sf::Keyboard::F1))
					{
					    gameMusic.stop();
						toMainMenu = true;
						if (musicMode)
                            menuMusic.play();
						break;
					}
					if(sf::Keyboard::isKeyPressed(sf::Keyboard::F2))
                    {
                        if (musicMode)
                            gameMusic.play();
						break;
                    }
				}

				if(toMainMenu){
					return false;
				}
				clock.restart();
			}
			else
				dt = clock.restart();

    		//sf::Time dt = clock.restart();
        	window.clear();
   			map.drawMap(window);
           	inventory.drawInventory(window, player);

            int enemyingrndrange = 0; // Muuttuja, joka tarkastaa, onko yksikään maavihollinen jonkun ground tornin alueella
			if(!towers.isEmpty())
            {
                for(auto iter = towers.getTowers().begin(); iter != towers.getTowers().end(); iter++)
                {

                    (*iter)->draw(window);

					if (!activeEnemies.isEmpty() && !gameEnded)
                    {

                        sf::Vector2f towerCenter = (*iter)->getCenter();
                        for(std::list<Enemy*>::iterator iter2 = activeEnemies.getEnemies().begin(); iter2 != activeEnemies.getEnemies().end(); iter2++)
                        {
                            sf::Vector2f enemyCenter = (*iter2)->getCenter();

                            float length = sqrt(pow(enemyCenter.x - towerCenter.x, 2) + pow(enemyCenter.y - towerCenter.y, 2));



                            if (length < (*iter)->getRange() && (*iter)->check_Enemy(*(*iter2)))
                            {
                                if ((*iter)->get_type() == 1) // HUOM! siirretty tähän
                                {// Pitää äänet päällä jos maatornien alueella vihuja
                                        if (MGsounds.empty()) // UUTTA: ääni
                                        {
                                            MGsounds.push_back(warSound);
                                            warSound.play();
                                        }
                                        enemyingrndrange = 1; // Huom
                                }

                                if ((*iter)->canShoot())
                                {
                                    (*iter)->shoot();
                                    if ((*iter)->get_type() == 1) // Debug, poistaa räjähdyksen muilta kuin MG kudeilta
                                        explosions.push_back(new Explosion(enemyCenter.x-32, enemyCenter.y-32));

                                    projectiles.addProjectile(new Projectile(enemyCenter, towerCenter, (*iter)->get_type(), (*iter)->getPower()));

                                    if ((*iter)->get_type() == 1)
                                        (*iter2)->get_damage((*iter)->make_Damage());

                                    activeEnemies.check_HP(iter2, player);


                                }
                                break;
                            }
                        }
                    }
                }

            }
            if (!MGsounds.empty() && !enemyingrndrange) // Lopeta äänentoisto
            {
                warSound.stop();
                MGsounds.pop_back();
            }

			if(!activeEnemies.isEmpty()){
				auto iter = activeEnemies.getEnemies().begin();
				while(iter != activeEnemies.getEnemies().end())
	   				{
	   					(*iter)->update(dt.asSeconds(), map.getObjMap());
	   					(*iter)->draw(window);
	   					(*iter)->draw_HP(window);
						int currHP = player.getHP();
						sf::Vector2i enemyCenter((*iter)->getCenter().x, (*iter)->getCenter().y);
						iter = activeEnemies.check_Finnish(iter, map.getObjMap(), player);

						int newHP = player.getHP();
						if (currHP > newHP && player.getHP() == 0)
						{
							Explosion *hugeExplosion = new Explosion;
							hugeExplosion->giveCoordinates(enemyCenter);
							hugeExplosion->setSequence(0,0);
							hugeExplosion->resetCounter();
							bombExplosion = true;
							explosions.push_back(hugeExplosion);

							//explosions.push_back(new Explosion(enemyCenter.x, enemyCenter.y, 2)); // BigBoom

							bassBombSound.play();
						}
						else if (currHP > newHP)
						{
							explosions.push_back(new Explosion(enemyCenter.x-32, enemyCenter.y-32, 1)); // Boom
							explosionSound.play();
						}
	   				}
			}





   			if(!projectiles.isEmpty())
			{
				for(auto iter = projectiles.getProjectiles().begin(); iter != projectiles.getProjectiles().end(); iter++)
				{
					sf::Vector2f targetCenter = sf::Vector2f(0,0);
					sf::Vector2f projPos = (*iter)->getCenter(); // Tarkistaa projectilen positionin

					if ((*iter)->missileTimeout())
					{
						explosions.push_back(new Explosion(projPos.x-64, projPos.y-64, 1)); // Boom // 32pix korjaus koska getCenter laskee rectanglen keskipisteen väärin
						explosionSound.play();
						(*iter)->setPast();
						continue;
					}

					if ((*iter)->get_type() == 2) // Ala tarkistamaan missilejen tilannetta /Markus 25.11.
					{
						if (!activeEnemies.isEmpty())
						{
							for(std::list<Enemy*>::iterator iter2 = activeEnemies.getEnemies().begin(); iter2 != activeEnemies.getEnemies().end(); iter2++)
							{
                                sf::Vector2f enemyCenter = (*iter2)->getCenter(); // Laskee vihujen etäisyydet

                                float length = sqrt(pow(enemyCenter.x+32 - projPos.x, 2) + pow(enemyCenter.y+32 - projPos.y, 2)); // 32pix korjaus koska getCenter laskee rectanglen keskipisteen väärin

                                if (length < (*iter)->getRange() && (*iter)->check_Enemy(*(*iter2)))
                                {
									if ( length < 20 ) // Jos ohjus on todella lähellä vihua, aiheuta siihen lämä
									{

										(*iter)->setPast();
										explosions.push_back(new Explosion(enemyCenter.x-32, enemyCenter.y-32, 1)); // Boom
										explosionSound.play();
										(*iter2)->get_damage((*iter)->make_Damage()); // Lämä
										activeEnemies.check_HP(iter2, player);
										break;
									}
									targetCenter = enemyCenter; // Syöttää vihun senhetkisen paikan
									break;
								}
							}
						}
					}

					(*iter)->update(dt.asSeconds(), targetCenter);

					if ((*iter)->get_type() == 3 && (*iter)->isPast()) // Tarkista area-pommien tilannetta /Markus 27.11.
					{
						if (!activeEnemies.isEmpty())
						{
							for(std::list<Enemy*>::iterator iter2 = activeEnemies.getEnemies().begin(); iter2 != activeEnemies.getEnemies().end(); iter2++)
							{
                                sf::Vector2f enemyCenter = (*iter2)->getCenter(); // Laskee vihujen etäisyydet

                                float length = sqrt(pow(enemyCenter.x+32 - projPos.x, 2) + pow(enemyCenter.y+32 - projPos.y, 2)); // 32pix korjaus koska getCenter laskee rectanglen keskipisteen väärin

                                if (length < (*iter)->getRange() && (*iter)->check_Enemy(*(*iter2))) // Damage to all enemies in range
                                {

									(*iter2)->get_damage((*iter)->make_Damage()); // Lämä
								}
							}
							activeEnemies.checkAllHP(player);

						}
						explosions.push_back(new Explosion(projPos.x, projPos.y, 2)); // BigBoom
                        bombSound.play();
					}

   					(*iter)->draw(window);
				}
				projectiles.updateProjectiles();
			}

   			drawPlayerStats();

			if(activeEnemies.isEmpty() && pauseBetweenWaves == true){
					newWave(window);
            }


			if(player.getHP() == 0)
			{
				gameEnded = true ;
				player.removeBombs(true);
				gameOver(window);
			}

			if (gameEnded == true)
			{
				gameMusic.stop();
				if (!MGsounds.empty()) // Lopeta äänentoisto
				{
					warSound.stop();
					MGsounds.pop_back();
				}
			}


    		if (events.checkEvents(window, towers, map, player, inventory, effects, activeEnemies, gameEnded) == false) // False = player has ended game
			{
				// Highscorejen lataaminen 25.11. -Lauri
				player.addPoints(player.getHP() * 5 * difficultyMultiplier);
				hscore.clearScores();
				hscore.loadScores(map.getMapName());
				hscore.addScore(player.getName(), player.getPoints());
				hscore.saveScores(map.getMapName());
				break;
			}

    		//debug
    		//std::cout << spawnTime.asSeconds() << std::endl ;


   			//UUSI 6.12. -Lauri
			if(pauseBetweenWaves)
			{
				if(spawnTime.asSeconds() > 10){
					pauseBetweenWaves = false;
					enemyWave = enemyWaves.back();
					enemyWaves.pop_back();
				}
			}
			else{
				if((enemyWave->isEmpty() && activeEnemies.isEmpty())){

					if(enemyWaves.empty() && enemyWave->isEmpty() && activeEnemies.isEmpty()){
						gameOver(window);
						gameEnded = true ;
					}
					else{
						spawnTime = sf::seconds(0);
						pauseBetweenWaves = true ;
					}

				}

				else if(!enemyWave->isEmpty() && spawnTime.asSeconds() > 2){
						spawnTime = sf::seconds(0);
						activeEnemies.makeEnemies(enemyWave->getEnemies().back()->get_Type(),map.getStart(), difficultyMultiplier);
						enemyWave->getEnemies().pop_back();
				}
			}

			spawnTime += dt;


			for(auto iter = explosions.begin(); iter != explosions.end(); iter++)
			{
				if((*iter)->getType() == 3)
				{
					//bigExplosion.animateBig(effects.explosionTex, window);

					(*iter)->animateBig(effects.explosionTex, window);

					if(!(*iter)->animateBig(effects.explosionTex, window)){

						bombExplosion = false ;
						delete *iter;
						iter = explosions.erase(iter);
						iter--;
					}
					continue;
				}

				(*iter)->animate(effects.explosionTex, window);
				if((*iter)->getSequenceX() > 60 || ((*iter)->getType() == 2 && (*iter)->getSequenceY() > 5) || ((*iter)->getType() == 0 && (*iter)->getSequenceX() > 14))
				{
					delete *iter;
					iter = explosions.erase(iter);
					iter--;
				}
			}


			window.display();


      }
      return true;
}




void Game::createWaves(){

		std::vector<std::vector<std::string>> WavesMap = map.getEnemyWaves();

		for(auto iter = WavesMap.rbegin(); iter != WavesMap.rend() ; iter++)
		{
			enemyWaves.push_back(new EnemyContainer());
			for( auto iter2 = (*iter).rbegin(); iter2 != (*iter).rend(); iter2++)
			{
				if((*iter2) == "g")
				{(enemyWaves.back())->makeEnemies(Ground,map.getStart(), difficultyMultiplier);}
				else if((*iter2) == "f")
				{(enemyWaves.back())->makeEnemies(Fly,map.getStart(), difficultyMultiplier);}
				else if((*iter2) == "G"){
					(enemyWaves.back())->makeEnemies(Ground_Boss,map.getStart(), difficultyMultiplier);}
				else if((*iter2) == "F"){
					(enemyWaves.back())->makeEnemies(Fly_Boss,map.getStart(), difficultyMultiplier);}
			}
		}
}





void Game::drawPlayerStats()
	{
        sf::Text player_name(player.getName(), font, 30);
        player_name.setPosition(850, 5);
        window.draw(player_name);

        std::string hp = "hp: " + toString(player.getHP());
        sf::Text player_hp(hp, font, 20);
        player_hp.setPosition(850, 40);
        window.draw(player_hp);

        std::string gold = "gold: " + toString(player.getGold());
        sf::Text player_gold(gold, font, 20);
        player_gold.setPosition(850, 60);
        window.draw(player_gold);

        std::string points = "points: " + toString(player.getPoints());
        sf::Text player_points(points, font, 20);
        player_points.setPosition(850, 80);
        window.draw(player_points);

        std::string bombs = "bombs: " + toString(player.getBombs());
        sf::Text player_bombs(bombs, font, 20);
        player_bombs.setPosition(850, 100);
        window.draw(player_bombs);

    }


void Game::newWave(sf::RenderTarget& rt)
{
	sf::Time temp = sf::seconds(10);
	temp = temp - spawnTime ;

	std::string temp_time =  toString(temp.asSeconds());
	std::string temp_time1 = temp_time.substr(0,1);

	sf::Text spawn_time("Get ready! New wave will start spawning in: " +  temp_time1, font, 30);
	spawn_time.setPosition(10, WHeight - 50);
	rt.draw(spawn_time);

}


void Game::gameOver(sf::RenderTarget& rt)
{
	if(player.getHP() == 0){
	sf::Texture gameOver;
    sf::RectangleShape gameover ;
 	gameOver.loadFromFile("graphics/gameover.png");
 	gameover.setSize(sf::Vector2f(557,150));
 	gameover.setTexture(&gameOver, true);
	gameover.setPosition(WWidth/2 - 557/2,WHeight/2 -150);
	rt.draw(gameover);}

	else{

	sf::Texture Victory;
    sf::RectangleShape victory ;
 	Victory.loadFromFile("graphics/victory.png");
 	victory.setSize(sf::Vector2f(557,150));
 	victory.setTexture(&Victory, true);
	victory.setPosition(WWidth/2 - 557/2,WHeight/2 -150);
	rt.draw(victory);}

}

