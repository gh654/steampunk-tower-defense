#ifndef PLAYER_HH
#define PLAYER_HH


#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <iostream>

class Player{

public:

	Player();
	
	~Player();
	
	bool reduceHP(int);
	
	void reduceGold(int);
	
	bool checkGold(int) const;
	
	void addGold(int);
	
	void addPoints(int);
	
	void setName(const std::string& name) ;
	
	std::string getName() const ;
	
	int getGold() const;
	
	int getPoints() const;
	
	int getHP() const;
	
	void addBomb();
	
	int getBombs() const;
	
	void removeBombs(bool = false); // True = remove all
	
	 //UUSI 28.11. -Lauri
	void reset();
	
private:

	Player(const Player& other) ;
	void operator=(const Player& other) ;



	int playerGold;
	int playerPoints;
	int playerHP;
	int playerBombs ;
	std::string playerName;

};


#endif
