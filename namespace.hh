#ifndef NAMESPACE
#define NAMESPACE
namespace
{
	const float gameAreaWidth = 832;
	const float WWidth = 1024;
	const float WHeight = 768;

	const float DMultEasy = 0.8;
	const float DMultNormal = 1.1;
	const float DMultHard = 1.5;
	const int EasyGold = 1000;
	const int NormalGold = 1200;
	const int HardGold = 1500;

	const int InvTowerX = 896;
	const int InvTowerXX = 960;
	const int InvMegabombXX = 986;
	const int InvGroundY = 224;
	const int InvGroundYY = 288;
	const int InvFlyingY = 290;
	const int InvFlyingYY = 354;
	const int InvSuperY = 360;
	const int InvSuperYY = 424;
	const int InvMegabombY = 450;
	const int InvMegabombYY = 514;

	const int InvBombButtonX = 870;
	const int InvBombButtonXX = 998;
	const int InvBombButtonY = 565;
	const int InvBombButtonYY = 699;

	const int GroundRange = 200;
	const int AirRange = 240;
	const int SuperRange = 280;
	const int AreaBombRange = 200;

	const int GroundESpeed = 50;
	const int GroundBossESpeed = 30;
}
#endif
