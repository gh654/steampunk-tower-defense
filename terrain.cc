#include"object.hh"
#include"terrain.hh"


//GRASS = 0, PATH = 1, START=2, FINISH=3, TREE=4
Terrain::Terrain(int Type, int X = 0, int Y = 0) :  Object(WTile, HTile), type(Type), posX(X), posY(Y)
  {		//GRASS = 0, PATH = 1, START=2, FINISH=3, TREE=4
        if(Type == 0){
                terrainTex.loadFromFile("graphics/terrain.png");
               	rect.setSize(sf::Vector2f(64,64));
                rect.setTexture(&terrainTex, true);
                rect.setTextureRect(sf::IntRect(0, 0, 64, 64));
               }
        else if(Type == 1){
                terrainTex.loadFromFile("graphics/terrain.png");
                rect.setSize(sf::Vector2f(64,64));
                rect.setTexture(&terrainTex, true);
                rect.setTextureRect(sf::IntRect(64, 128, 64, 64));        
                }
        else if(Type == 2){
                terrainTex.loadFromFile("graphics/terrain.png");
               	rect.setSize(sf::Vector2f(64,64));
                rect.setTexture(&terrainTex, true);
                rect.setTextureRect(sf::IntRect(64, 128, 64, 64));        
        		}   
        		
       	else if(Type == 3){
       	        terrainTex.loadFromFile("graphics/terrain.png");
               	rect.setSize(sf::Vector2f(64,64));
                rect.setTexture(&terrainTex, true);
                rect.setTextureRect(sf::IntRect(128, 128, 64, 64));  
       			}
       	else{
       	       	terrainTex.loadFromFile("graphics/terrain.png");
               	rect.setSize(sf::Vector2f(64,64));
                rect.setTexture(&terrainTex, true);
                rect.setTextureRect(sf::IntRect(64, 0, 64, 64));  
       			}            
  //mouseOver.loadFromFile("graphics/canBuild.png"); Ei tarvita enää 25.11. -Lauri

  rect.setPosition(64*X, 64*Y);
}
  
  
void Terrain::draw(sf::RenderTarget &rt) const
{
  rt.draw(rect);
}

  
// Ei tarvita enää 25.11. -Lauri
/*void Terrain::updateTex()
{
   if(type == 0)
   *              terrainTex.loadFromFile("graphics/grass.png");
   *      else
   *              terrainTex.loadFromFile("graphics/path.png");
   *             
   *      rect.setPosition(64*posX,64*posY);
  if(rect.getTexture() == &mouseOver){
  	if(type == 0){
  	  rect.setTexture(&terrainTex, true);
  	  rect.setTextureRect(sf::IntRect(0, 0, 64, 64));}
  	else if(type == 4){
  	  rect.setTexture(&terrainTex, true);
      rect.setTextureRect(sf::IntRect(64, 0, 64, 64));}
    }
}*/


// 1 = sivuttain, 2 = ylös-alas, 3 = vasen alas, 4 = oikea alas, 5 = vasen ylös, 6 = oikea ylös
void Terrain::adjustPaths(int pathType)
{
	switch(pathType){
	
		case 1:
			rect.setTextureRect(sf::IntRect(128, 0, 64, 64));
			break;
		case 2:
			rect.setTextureRect(sf::IntRect(192, 0, 64, 64));
			break;
		case 3:
			rect.setTextureRect(sf::IntRect(64, 64, 64, 64));
			break;
		case 4:
			rect.setTextureRect(sf::IntRect(0, 64, 64, 64));
			break;
		case 5:
			rect.setTextureRect(sf::IntRect(128, 64, 64, 64));
			break;
		case 6:
			rect.setTextureRect(sf::IntRect(192, 64, 64, 64));
			break;
			}
}


int Terrain::getPosX() const
{
  return posX ;
}

  
int Terrain::getPosY() const
{
  return posY;
}
  
int Terrain::getType() const
{
  return type;
}

void Terrain::setType(int t)
{
	type = t;
}

// Ei tarvita enää 25.11. -Lauri
/*void Terrain::changeTex(int tileType){

  rect.setTexture(&mouseOver, true);
  if(tileType == 0){
  	rect.setTextureRect(sf::IntRect(0, 0, 64, 64));}
  else if(tileType == 4){
  	rect.setTextureRect(sf::IntRect(64, 0, 64, 64));}
}*/
