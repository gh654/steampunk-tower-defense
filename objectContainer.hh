#ifndef OBJECTCONTAINER_HH
#define OBJECTCONTAINER_HH

#include "object.hh"
#include "map.hh"
#include "enemy.hh"
#include "tower.hh"
#include "terrain.hh"
#include "projectile.hh"
#include "player.hh"
#include <list>
#include <string>
#include <iostream>


class EnemyContainer{

public:

	 EnemyContainer();
	  
	 ~EnemyContainer();
		
	 std::list<Enemy*>& getEnemies();
	  
	  
	void addEnemy(Enemy*);
	 
	void deleteEnemy(std::list<Enemy*>::iterator);
	
	void updateLast();
	
	bool isEmpty();
	
	void makeEnemies(EnemyType, sf::Vector2i, float);
	/*std::list<Enemy*>::iterator findDead();
	 */
	
	//void updateWave(EnemyType, Map &); // Funkiota ei käytetä missään ??
	
	void updateWave(Map &);
	
	
	void check_HP(std::list<Enemy*>::iterator, Player& player);
	
	void checkAllHP(Player& player);
	
	void clearContainer();  //UUSI 28.11. -Lauri
	
	//Checks if the enemy has made it to the finnishline
	std::list<Enemy*>::iterator check_Finnish( std::list<Enemy*>::iterator, std::vector<std::vector<Terrain*>> const &, Player&);
	

	
private:


	std::list<Enemy*> enemyList;
	EnemyContainer(const EnemyContainer& other) ;
	void operator=(const EnemyContainer& other) ;

};




class TowerContainer{

public:

	TowerContainer();
	  
	~TowerContainer();
		
	std::list<Tower*>& getTowers();
	  
	  
	void addTower(Tower*);
	 
	void deleteTower(std::list<Tower*>::iterator);
	 
	void updateLast(int type);
	 
	bool isEmpty();
	
	
	
	void addTowers(int, int, int type);
	void clearContainer();  //UUSI 28.11. -Lauri

	
private:


	std::list<Tower*> towerList;
	TowerContainer(const TowerContainer& other) ;
	void operator=(const TowerContainer& other) ;


};



class ProjectileContainer{

public:

	  ProjectileContainer();
	  
	  
	  ~ProjectileContainer();
		
	  std::list<Projectile*>& getProjectiles();
	  
	  
	 void addProjectile(Projectile* new_proj);
	 
	 void updateProjectiles();
	 
	 void updateLast();
	 
	bool isEmpty();
	void clearContainer();  //UUSI 28.11. -Lauri
	

	
private:
	std::list<Projectile*> projList;
	ProjectileContainer(const ProjectileContainer& other) ;
	void operator=(const ProjectileContainer& other) ;

};





#endif
