#include "player.hh"


	Player::Player()
	{
		playerHP = 100;
		playerGold = 0 ;
		playerPoints = 0;
		playerBombs = 0 ;
	}

	Player::~Player()
	 {}

	void Player::setName(const std::string& name)
	{
		playerName = name;
	}

	bool Player::reduceHP(int damage)
	{
		if(damage < playerHP)
		{
			playerHP -= damage;
			return true;
		}
		else{
			playerHP = 0;
			return false ;
		}
	}

	void Player::reduceGold(int amount)
	{
		if(amount < playerGold)
		{
			playerGold -= amount ;
		}
		else
			playerGold = 0 ;
	}

	bool Player::checkGold(int towerType) const
	{
		bool enoughGold = (false);

		if( towerType == 1)
		{
			if(playerGold >= 100)
			{
				enoughGold = (true);
			}
		}
		else if( towerType == 2)
		{

			if(playerGold >= 200)
			{
			enoughGold = (true);
			}

		}
		else
		{
			if(playerGold >= 700){
			enoughGold = (true);
			}
		}

	return enoughGold;
	}


	void Player::addGold(int amount)
	{
		playerGold += amount;
	}


	void Player::addPoints(int amount)
	{
		playerPoints += amount ;
	}

	std::string Player::getName() const
	{
		return playerName;
	}


	int Player::getGold() const
	{
		return playerGold;
	}

	int Player::getPoints() const
	{
		return playerPoints;
	}


	int Player::getHP() const
	{
		return playerHP;
	}

	int Player::getBombs() const
	{
		return playerBombs ;
	}

	void Player::addBomb()
	{
		playerBombs++ ;
	}

	void Player::removeBombs(bool removeAll)
	{
		if (removeAll)
			playerBombs = 0;
		else
			playerBombs--;
	}

 //UUSI 28.11. -Lauri
void Player::reset()
{
		playerHP = 100;
		playerGold = 0 ;
		playerPoints = 0;
		playerBombs = 0;
		playerName = "";
}
