
#include"tower.hh"




Tower::Tower(int type, int X = 0, int Y = 0) : Object(Wtower, Htower),type(type), posX(X), posY(Y)
 {
    level = 1; //Arber 25.11.
  switch (type){
      case 1:
	range = GroundRange;
	Power =1;
	price = 100; //Arber 25.11.
	uppgradePrice = 200;
	reload = 0.2;

	break;

      case 2:
	range = AirRange;
	Power = 6;
	price = 200; //Arber 25.11.
	uppgradePrice = 400;
	reload = 2;
	break;

      default:
	range = SuperRange;
	Power = 20;
	price = 700; //Arber 25.11.
	uppgradePrice = 1000;
	reload = 10;
	break;
    }

 }

 void Tower::draw(sf::RenderTarget &rt) const
 {
   rt.draw(rect);
 }


void Tower::updateTex(int type)
{
  if(type == 1)
  {
    switch(level){
      case 1:
	towerTex.loadFromFile("graphics/land_tower.png");
	break;
      case 2:
	towerTex.loadFromFile("graphics/land_tower_level_2.png");
	break;
      case 3:
	break;

      case 4:
	break;
    }


    rect.setPosition(64*posX,64*posY);
    rect.setTexture(&towerTex, true);
  }
  else if(type == 2)
  {
    switch(level){
      case 1:
	towerTex.loadFromFile("graphics/air_tower.png");
	break;
      case 2:
	towerTex.loadFromFile("graphics/air_tower_level_2.png");
	break;
      case 3:
	break;
      case 4:
	break;

    }

    rect.setPosition(64*posX,64*posY);
    rect.setTexture(&towerTex, true);

  }
  else
  {
    switch(level){
      case 1:
	towerTex.loadFromFile("graphics/super_tower.png");
	break;
      case 2:
	towerTex.loadFromFile("graphics/super_tower_level_2.png");
	break;
      case 3:
	break;
      case 4:
	break;

    }
    rect.setPosition(64*posX,64*posY);
    rect.setTexture(&towerTex, true);

  }


}


bool Tower::canShoot()
{
  if (reloadTime.getElapsedTime().asSeconds() > reload)
  {
    return true;
  }

  return false;
}

void Tower::shoot()
{
    reloadTime.restart();
}


int Tower::getRange()
{
  return range;

}



int Tower::getPosX() const
{
  return posX ;
}

int Tower::getPosY() const
{
  return posY;
}

//seuraavat 4 funktiota muokattu 21.11 ~Arber
int Tower::make_Damage()const{
  return Power;

}

int Tower::get_type()const{
  return type;
}

int Tower::getPower() const
{
	return Power;
}

bool Tower::check_Enemy(Enemy enemy){
  switch (get_type()){
    case 1:
      if (enemy.get_Type() == Ground || enemy.get_Type() == Ground_Boss)
	return true;
      else
	return false;
      break;
    case 2:
      if (enemy.get_Type() == Fly || enemy.get_Type() == Fly_Boss)
	return true;
      else
	return false;
      break;
    default:
      return true;
  }
}


int Tower:: get_level()const{//Arber 25.11
return level;
}

int Tower:: get_Price()const{//Arber 25.11
return price;
}

int Tower::get_uppgradePrice()const{ //Arber 28.11
  return uppgradePrice;
}

void Tower::upgrade(){ //Modattu 8.12.
	level++;

	switch(type)
	{
		case 1:
			range += 10;
			Power =1;
			price = price + uppgradePrice;
			uppgradePrice += 100;
			reload *= 0.75;
			break;

		case 2:
			Power += 1;
			price = price + uppgradePrice;
			uppgradePrice += 200;
			reload *= 0.8;
			break;

		case 3:
			range += 20;
			Power += 10;
			price = price + uppgradePrice;
			uppgradePrice += 500;
			reload -= 1;
			if (reload < 1) reload = 1;
			break;
	}

}




