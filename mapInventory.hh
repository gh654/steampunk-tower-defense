#ifndef MAPINVENTORY_HH
#define MAPINVENTORY_HH

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include "namespace.hh"

#include <iostream>
#include <string>

#include"player.hh"


class MapInventory{

public:

	MapInventory();
	
	~MapInventory();
	
	
	void drawInventory(sf::RenderTarget& rt, Player& player);
	
	void drawTowerInfo(sf::Vector2i mousePos, sf::RenderTarget& rt, Player&);
	
	int get_Player_Money(Player player)const;
	

	
private:

	MapInventory(const MapInventory& other) ;
	void operator=(const MapInventory& other) ;
	sf::Texture InventoryTex;
	sf::Sprite InventoryBox ;
	sf::Font info_font ; // 19.11. Markus
	
	

	
};

int checkTowerSelection(sf::Vector2i& mousePos);
int towerIntroduction(sf::Vector2i& mousePos);

#endif
