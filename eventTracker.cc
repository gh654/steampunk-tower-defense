

#include "eventTracker.hh"



//EventTracker::EventTracker(bool tmode = false) : towermode(tmode)
EventTracker::EventTracker()
{
  tex.loadFromFile("graphics/mouseTarget.png");
  rect.setTexture(&tex, true);
  towermode = false;
  bombmode = false ;
  bombExplosion = false;
  upp_font.loadFromFile("steampunk.ttf"); //Arber 25.11.
  checker = false;//Arber 25.11.
  towerRange = 0;
  uppBoxX=0;//Arber 25.11.
  uppBoxY=0;//Arber 25.11.
  uppBoxPrice=0;//Arber 25.11.
  uppgradePrice=0;//Arber 25.11.
  uppBoxNXT=0;//Arber 25.11.

  gameEnded = false;

  bassBombSndBuff.loadFromFile("audio/Explosion_Ultra_Bass.wav");
  bassBombSound.setBuffer(bassBombSndBuff); // Ison pommin äänet tällä hetkellä tässä, onko hyvä paikka?

  selectedTowerType = 0;
}

EventTracker::~EventTracker()
{}

void EventTracker::draw_upp_box(sf::RenderWindow& window,  Player &player){ //Arber 26.11

  std::string str_nxt_level = toString(uppBoxNXT);


    std::string str1 = "Upgrade";
    std::string str = "            to level: ";
    str.append(str_nxt_level);
    str.append("\nCost: ");
    str.append(toString(uppgradePrice));

    str.append("\n         (");
    std::string str2 = "Sell";
    str.append(toString(uppBoxPrice/2));
    str.append(")");
    //std::cout<<toString(mousePos.y)<<std::endl;

    sf::Text upp_info3(str2, upp_font, 20);

    sf::Text upp_info2(str1, upp_font, 20);
    if (player.getGold() < uppgradePrice)
      upp_info2.setColor(sf::Color::Red);
    else
      upp_info2.setColor(sf::Color::Green);

    upp_info3.setColor(sf::Color::Red);


    sf::Text upp_info(str, upp_font, 20);
    sf::RectangleShape upp_shape;
    upp_shape.setFillColor(sf::Color::Black);
    upp_shape.setSize(sf::Vector2f(205,75));
    upp_shape.setPosition(uppBoxX,uppBoxY-25);
    upp_shape.setOutlineColor (sf::Color::White);
    upp_shape.setOutlineThickness(5);
    window.draw(upp_shape);

    upp_info3.setPosition(uppBoxX+ 10,uppBoxY +22);
    window.draw(upp_info3);

    upp_info2.setPosition(uppBoxX+ 10,uppBoxY-25);
    window.draw(upp_info2);

    upp_info.setPosition(uppBoxX+ 10,uppBoxY-25);
    window.draw(upp_info);

	sf::CircleShape rangeCircle; // Draw range
	rangeCircle.setFillColor(sf::Color::Transparent);
	rangeCircle.setOutlineColor(sf::Color::Red);
	rangeCircle.setOutlineThickness(2);

	rangeCircle.setPosition((uppBoxX / 64)*64-towerRange+32, (uppBoxY / 64)*64-towerRange+32);
	rangeCircle.setRadius(towerRange);
	window.draw(rangeCircle);

}

bool EventTracker::get_checker()const{ //Arber 26.11
  return checker;}



void EventTracker::uppgrade_Bar(sf::Event &event, std::list<Tower*>& tow){ // Arber 25.11


if(event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Right){

  if(check_tower(tow, mousePos))
    checker = true;
  else
    checker = false;
}


}

bool EventTracker::check_tower(std::list<Tower*>& tow, sf::Vector2i &mousePos){ //Arber 26.11
  int towerPosX;
  int towerPosY;
  int mousePosX = mousePos.x / 64;
  int mousePosY = mousePos.y / 64;



  for (auto iter = tow.begin(); iter != tow.end(); iter++)
  {
    towerPosX = (*iter)->getPosX();
    towerPosY = (*iter)->getPosY();


    if (mousePosX == towerPosX && mousePosY == towerPosY) {
      uppBoxX = mousePos.x;
      uppBoxY = mousePos.y;
      uppBoxNXT = (*iter)->get_level()+1;
      uppBoxPrice = (*iter)->get_Price();
      uppgradePrice = (*iter)->get_uppgradePrice();

		towerRange = (*iter)->getRange();
      return true;
    }
  }
  return false;
}


void EventTracker::sell(Player &player,TowerContainer& toww, Map& map){

 for (auto iter = toww.getTowers().begin(); iter != toww.getTowers().end(); iter++){
   if(uppBoxX / 64 == (*iter)->getPosX() && (*iter)->getPosY() == uppBoxY / 64){
     auto it = iter;

	 int gridX = (*iter)->getPosX();
	 int gridY = (*iter)->getPosY();
     iter++;
     toww.deleteTower(it);
     player.addGold(uppBoxPrice/2);

	  auto objMap = map.getObjMap();
	  objMap[gridY][gridX]->setType(0);
   }
 }
}


void EventTracker::check_left_click(sf::Event &event, Player &player, TowerContainer& towers, Map& map){ //Arber 26.11
  if(event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left){
    if(mousePos.x <uppBoxX + 70 && mousePos.x > uppBoxX + 10 && mousePos.y > uppBoxY-25 && mousePos.y < uppBoxY +8)
      uppgrade(player, towers);
    if(mousePos.x <uppBoxX + 70 && mousePos.x + 10 > uppBoxX && mousePos.y > uppBoxY +27 && mousePos.y < uppBoxY +47)
      sell(player, towers, map);
    checker = false;
  }

}


void EventTracker::uppgrade(Player &player, TowerContainer& toww){
  for (auto iter = toww.getTowers().begin(); iter != toww.getTowers().end(); iter++){
    if(uppBoxX / 64 == (*iter)->getPosX() && (*iter)->getPosY() == uppBoxY / 64){
      if(player.getGold() >= (*iter)->get_uppgradePrice()){
	player.reduceGold((*iter)->get_uppgradePrice());
	(*iter)->upgrade();
	(*iter)->updateTex((*iter)->get_type());
      }
    }
  }
}







bool EventTracker::checkEvents(sf::RenderWindow& window, TowerContainer& towers, Map& map, Player& player, MapInventory& inventory, Effects& effects, EnemyContainer& enemies, bool gameEnd)
{
  sf::Event event;

  gameEnded = gameEnd;


  mousePos = sf::Mouse::getPosition(window); // Mousepos siirretty tähän

  if(mousePos.x > gameAreaWidth && towermode == false)
  {
    // uusi funktio 17.11. rikki -> 19.11. funktion täytyy olla tässä /Markus
    inventory.drawTowerInfo(mousePos, window, player) ;
  }

  while (window.pollEvent(event))
  {
	  if (event.type == sf::Event::KeyPressed)
	  {
		switch (event.key.code)
		{
			case sf::Keyboard::Num1: // Tornien pikanäppäimet, Markus 3.12.
				selectedTowerType = 1;
				break;
			case sf::Keyboard::Num2:
				selectedTowerType = 2;
				break;
			case sf::Keyboard::Num3:
				selectedTowerType = 3;
				break;
			case sf::Keyboard::Space:
				if(gameEnded == true)
				{
					return false;
					//window.close();
				}
				break;
			default:
				selectedTowerType = 0;
				break;
		}

		if(selectedTowerType != 0 && selectedTowerType != 4 && selectedTowerType != 5)
	    {
	      if(player.checkGold(selectedTowerType))
	      {
			towermode = true;
	      }
	    }
	  }



    if(!get_checker())//Arber 26.11
      uppgrade_Bar(event, towers.getTowers());//Arber 26.11

	if(get_checker())
	  check_left_click(event, player, towers, map);//Arber 26.11



      if(event.type == sf::Event::Closed)
      {
	window.close();
      }
      if(mousePos.x > gameAreaWidth && towermode == false)
      {
	// uusi funktio 17.11. rikki
	//inventory.drawTowerInfo(mousePos, window, player.getGold()) ;

	if(event.type == sf::Event::MouseButtonPressed)
	{
	  if(event.mouseButton.button == sf::Mouse::Left)
	  {
	    // uusi funktio 16.11
	    selectedTowerType = checkTowerSelection(mousePos);

	    if(selectedTowerType != 0 && selectedTowerType != 4 && selectedTowerType != 5)
	    {
	      if(player.checkGold(selectedTowerType))
	      {
			towermode = true;
	      }
	    }

	    else if(selectedTowerType == 4 && player.getGold() >= 1000)
	    {
	      player.addBomb() ;
		  player.reduceGold(1000);
	    }

	    else if(selectedTowerType == 5 && player.getBombs() > 0)
	    {
	      bombmode = true ;
	    }
	  }

	}
      }

      mousePos = sf::Mouse::getPosition(window);
      if (event.type == sf::Event::MouseButtonPressed  && towermode == true)
      {
	mousePos = sf::Mouse::getPosition(window);
	if (mousePos.x < gameAreaWidth)
	{
	  int gridX = mousePos.x/64;
	  int gridY = mousePos.y/64;

	  auto objMap = map.getObjMap();
	  if(objMap[gridY][gridX]->getType() == 0){

	    if(event.mouseButton.button == sf::Mouse::Left)
	    {
	      towers.addTowers(mousePos.x, mousePos.y, selectedTowerType);
		  objMap[gridY][gridX]->setType(4);
	      towermode = false;
	      if(selectedTowerType == 1)
		player.reduceGold(100);
	      else if(selectedTowerType == 2)
		player.reduceGold(200);
	      else if(selectedTowerType == 3)
		player.reduceGold(700);
	    }
	  }

	}
      }

      else if(event.mouseButton.button == sf::Mouse::Left && bombmode == true && mousePos.x < gameAreaWidth)
      {
	bassBombSound.play();
	player.removeBombs();

	bombExplosion = true ;
	bombmode = false ;
	mousePos = sf::Mouse::getPosition(window);
	bigExplosion.giveCoordinates(mousePos);
	bigExplosion.setSequence(0,0);
	bigExplosion.resetCounter();

	for(std::list<Enemy*>::iterator iter = enemies.getEnemies().begin(); iter != enemies.getEnemies().end(); iter++){
	  sf::Vector2i enemyPos ;
	  sf::Vector2i distance ;
	  int hypotenuse ;

	  enemyPos.x = (*iter)-> getPosPixelX();
	  enemyPos.y = (*iter)-> getPosPixelY();
	  distance.x = abs(mousePos.x - enemyPos.x);
	  distance.y = abs(mousePos.y - enemyPos.y);

	  hypotenuse = sqrt(distance.x * distance.x + distance.y * distance.y);

	  if(hypotenuse < 256){
	    (*iter)-> get_damage(60);

	  }

	}
	enemies.checkAllHP(player);
      }

      else if(event.mouseButton.button == sf::Mouse::Right && (bombmode == true || towermode == true))
      {
	bombmode = false ;
	towermode = false ;
      }

  }
  // Muutettu 25.11. -Lauri

  if (towermode == true)
  {
    mousePos = sf::Mouse::getPosition(window);
    if (mousePos.x < gameAreaWidth && mousePos.x >= 0 && mousePos.y >= 0 && mousePos.y < WHeight)
    {
		sf::CircleShape rangeCircle;
		rangeCircle.setFillColor(sf::Color::Transparent);
		rangeCircle.setOutlineColor(sf::Color::Red);
		rangeCircle.setOutlineThickness(2);
		int range;
		switch(selectedTowerType)
		{
			case 1:
				range = GroundRange;
				break;
			case 2:
				range = AirRange;
				break;
			case 3:
				range = SuperRange;
				break;
		}
		rangeCircle.setPosition((mousePos.x/64)*64-range+32, (mousePos.y/64)*64-range+32);
		rangeCircle.setRadius(range);
		window.draw(rangeCircle);

      int gridX = mousePos.x/64;
      int gridY = mousePos.y/64;

      auto objMap = map.getObjMap();
      int tileType = objMap[gridY][gridX]->getType() ;
	if(tileType == 0)
	  rect.setTextureRect(sf::IntRect(0, 0, 64, 64));
	else
	  rect.setTextureRect(sf::IntRect(64, 0, 64, 64));


	sf::Vector2f size(64,64);
	rect.setPosition(gridX*64, gridY*64);
	rect.setSize(size);
	window.draw(rect);
	//objMap[gridY][gridX]->changeTex(tileType);

    }
  }



  if(bombExplosion == true)
  {
    //bigExplosion.animateBig(effects.explosionTex, window);

    bigExplosion.animateBig(effects.explosionTex, window);

    if(!bigExplosion.animateBig(effects.explosionTex, window)){

      bombExplosion = false ;
      bombmode = false ;
    }
  }




  mousePos = sf::Mouse::getPosition(window);
  // Juha 26.11
  if(bombmode == true && towermode == false && (mousePos.x < gameAreaWidth))
  {
    rect.setSize(sf::Vector2f(512,512));
    rect.setTextureRect(sf::IntRect(0, 64, 512, 512));
    rect.setPosition(mousePos.x - 256, mousePos.y - 256);
    window.draw(rect);
  }

    if(get_checker())//Arber 26.11
      draw_upp_box(window, player);//Arber 26.11



	return true;
}





