#ifndef TOWER_HH
#define TOWER_HH
#include "object.hh"
#include "enemy.hh"
#include <vector>
#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>


class Tower : public Object
{
public:
  Tower(int, int,int);


  void draw(sf::RenderTarget &) const;


  void updateTex(int type);


  bool canShoot();

  void shoot();

  int getRange();

  int getPosX() const;

  int getPosY() const;

    int make_Damage()const;

  bool check_Enemy(Enemy);

  int get_type()const;

  int getPower() const;

    int get_level()const; //Arber 25.11

  int get_Price()const; //Arber 25.11

  int get_uppgradePrice()const; // 28.11

  void upgrade();// Modattu 8.12. (Vanha: set_attributes)


private:
  int type; // 1 - Ground, 2 - Air, 3 - Area
  int Power;
  int range;
  float reload; // Kertoo kauanko torni lataa
  sf::Clock reloadTime;
  int posX;
  int posY;
  sf::Texture towerTex ;
  int level; //Arber 25.11.
  int price; //Arber 25.11.
  int uppgradePrice; //Arber 27.11

};



#endif
