#ifndef OBJECT_HH
#define OBJECT_HH


#include <iostream>
#include <math.h>
#include <sstream>
#define PI 3.14159265
#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>


template <typename T>         //siirsin tämän funktion tänne nii kaikki luokat voi käyttää sitä: Arber 27.11
std::string toString(const T& v)
{
  std::ostringstream oss;
  oss << v;
  return oss.str();
}



namespace
{
	const float WEnemy =  64;
	const float HEnemy = 64;
	const float Wtower = 64;
	const float Htower = 64;
	const float WTile = 64;
	const float HTile = 64;
}



// Objekti nikkilän koodista.
class Object
{
public:
  Object(float w, float h) : rect(sf::Vector2f(w,h)), v(0,0)
  {}
  virtual ~Object() {}

  void draw(sf::RenderTarget &rt) const
  {
    rt.draw(rect);
  }

  const sf::RectangleShape& getRect() const { return rect; }


  sf::Vector2f getCenter() const { return sf::Vector2f(rect.getPosition().x + (WEnemy/2), rect.getPosition().y + (HEnemy/2)); };


protected:
  sf::RectangleShape rect;
  sf::Vector2f v; // <- jotain / s
};


/*
class Projectile : public Object
{
public:
    Projectile(sf::Vector2f enemypos, sf::Vector2f pos) : Object(10, 10), posX(pos.x), posY(pos.y), target(enemypos)
    {
        rect.setFillColor(sf::Color::Black);
        rect.setPosition(posX, posY); // Aseta keskelle tornitileä

		//target = enemy.getCenter();

		float vx = (target.x - posX);
		float vy = (target.y - posY);
		rect.rotate(180 / PI * atan(vy/vx));
        vx *= 10;
        vy *= 10;

        //std::cout << "Projectile vx " << vx << " vy " << vy << " Rotation " << rect.getRotation() << std::endl;

		v = sf::Vector2f(vx ,vy);

		pastTarget = false;
    }
    void update(float dt)
    {
		rect.move(dt * v);


		if ((posX - target.x) > 0) // Jos lähdetään oikealta
		{
			if ((posY - target.y) > 0) { // Jos lähdetään alhaalta targetiin nähden
				if (rect.getPosition().x <= target.x && rect.getPosition().y <= target.y) { // jos ollaan ylävasemmalla
					pastTarget = true;
				}
			}

			else // Lähdetään ylhäältä tai tasasta
				if (rect.getPosition().x <= target.x && rect.getPosition().y >= target.y) { // jos ollaan alavasemmalla
					pastTarget = true;
				}
		}
		else // Lähdetään vasemmalta tai tasasta
		{
			if ((posY - target.y) > 0) { // Jos lähdetään alhaalta targetiin nähden
				if (rect.getPosition().x >= target.x && rect.getPosition().y <= target.y) { // jos ollaan yläoikealla
					pastTarget = true;
				}
			}

			else // Lähdetään ylhäältä tai tasasta
				if (rect.getPosition().x >= target.x && rect.getPosition().y >= target.y) { // jos ollaan alaoikealla
					pastTarget = true;

				}
		}
    }

	bool isPast() // Onko projectile ohittanut kohteensa
		{return pastTarget;}

    void updateTex()
    {
		rect.setFillColor(sf::Color::Black);
		rect.setPosition(64*posX,64*posY);
    }
private:
    int posX;
	int posY;
	//Enemy& enemy; // Vihu itse
    sf::Vector2f target; // Vihun koordinaatit
	bool pastTarget;


};
*/

#endif






