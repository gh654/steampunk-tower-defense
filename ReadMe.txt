20.11. Markus 


- Vihujen animaatio toimii täydellisesti (enemy.cc ja .hh)

- Inventoryvalikon piirto toimii (mapInventory.cc)

- Koodia kommentoitu

- Alkuscreeni luotu (game.cc), siinä tosin joku ylimääräinen characteri ilmestyy kuvioihin
	phase 1:ssä
	
	
	
	
	
24.11. Juha

-Terrain.cc:ssä muutettu updateTex ja changeTex funktioita niin, että towermodessa tilet piirtyvät oikein
-Terrain.cc:ssä muutettu changeTex funktiota niin, että funktio ottaa sisäänsä nyt parametrin (ruutuun voi rakentaa, tai ei voi rakentaa)
-"Uusi" tekstuuri canBuild.png
-map.cc:ssä lisätty vihollisten lukemiseen tiedostosta G = ground boss ja F = flying boss
-main.cc:ssä uusi funktio createWaves, joka luo vektorin tyyppiä std::vector<EnemyContainer*> enemyWaves
-checkEvents funktiota eventTracker.cc:ssä muutettu, saa parametrina nyt EnemyContainer* enemies

-game.hh:ssa nyt kolme uutta säiliötä vihollisille:

	std::vector<EnemyContainer*> enemyWaves = sisältää kaikki yhden pelisession viholliaallot. Eli peli on voitettu kun tämä säiliö on tyhjä.
	
	EnemyContainer* enemyWave = sisältää tiedot yhden aallon vihollisista. Uusi aalto haetaan enemyWaves säiliöstä.
	
	EnemyContainer* activeEnemies = sisältää vain ne viholliset jotka tällä hetkellä ovat ruudulla. Eli tämä säiliö hakee sisältönsä enemyWave säiliöstä sitä mukaan kun vihollisia on määrätty spawnaamaan lisää ruudulle.
	
	
-vihollisten spawnaamiseen liittyvät toiminnot kaikki nyt main.cc:ssä.

-Enemy.cc:ssä uudet funktiot getPosPixelX ja getPosPixelY(), koska aikasemmin ei oltu tehty funktiota joka palauttaa pikselintarkan sijainnin

-uusi class Explosion ja Effect jotka ovat effects.cc:ssä.

-game.hh:ssa uudet private muuttujat 	Effects effects ja std::vector<Explosion*> explosions

-game.cc:ssä uusi loop joka käy läpi räjähädyksiä



25.11. Lauri
- Muutetut filet:
	- terrain.cc
		* updateTex() ja changeTex() funktiot poistettu
	- terrain.hh
		* updateTex() ja changeTex() funktiot poistettu
	- map.cc
		* drawMap() funktiosta updateTex() pois
	- objectContainer.cc
		* checkFinnish() funktioon vaihdettu oikea iteraattori kun kutsutaan reduceHP() funktiota
	- game.hh
		* lisätty scores() funktio ja hscore private muuttuja
	- game.cc
		* lisätty scores() funktio joka piirtää Highscore-näkymän, kutsutaan heti play() funktion jälkeen
		* play() funktio lataa pelin loputtua Highscore-tiedot tekstitiedostosta, lisää nykyisen pelaajan tiedot ja tallentaa 			listan
	- highscore.cc
		* Funktiot lataamiseen, tallentamiseen, pelaajan lisäämiseen, listan järjestämiseen, listan palauttamiseen
	- highscore.hh
		* Funktiot lataamiseen, tallentamiseen, pelaajan lisäämiseen, listan järjestämiseen, listan palauttamiseen
	- eventTracker.cc
		* Tornia rakennettaessa piirtää nyt tekstuurin ruutuun sen sijaan että terrain-objektin tekstuuria vaihdettaisiin
		
		
26.11. Markus

- Lisätty projectile.cc ja .hh, poistettu toteutus object.hh:sta

- Air towerit ampuvat nyt missilejä, (jotka pyörivät väkkärää, kewl), ja kaikki projectilet liikkuvat vakionopeudella
- Missilet suorittavat samanlaista iterointia ympärillä olevista vihollisista kuin tornit, toteutusta game.cc:ssä.
- Kopsattu missilen toteutukseen projectile.cc:hen myös useita funktioita tower.cc:stä, ja lisätty projectilejen tyypit.

- Korjattu alkuruudun bugi joka tulostaa neliön jos painaa enteriä.			
		
		
		
26.11. Juha

- eventTracker.cc:ssä towerModessa muutettu piirtämään canBuild.png, vihreä tai punainen ruutu
- mapInventory.hh päivitetty
- player.cc:hen lisätty "int bombs", getBombs(), addBomb()
- pari uutta spriteä
- effects.cc päivitetty, uusia funktioita
- eventTracker.cc nyt bombMode ja bombExplosion bool-valuet. Listätty myös if-else ehtoja eventteihin
- objectContainer.cc:ssä checkAllHP() funktio ;




28.11. Lauri

- game.hh & .cc
	* resetGame() funktio, palauttaa pelin alkutilaan
	* mainMenu() funktio, toimii pelin päävalikkona
	* start() funktioon pieniä graafisia muutoksia, "resettaa" event queuen funktion alussa, enteri toimii oikein siirryttäessä 		phasesta toiseen, peli resetataan funktion lopussa resetGame() kutsulla
	* scores() palaa main menuun
	* play() pause ominaisuus Escape näppäimestä, jonka avulla pelin voi myös lopettaa kesken, funktio palaa main menuun
- objectContainer.hh & .cc
	* Kaikille Containereille clearContainer() funktio joka tyhjentää containerin
- player.hh & .cc
	* reset() funktio palauttaa pelaaja olion arvot alkutilaan
- map.hh & .cc
	* clearMap() funktio joka tyhjentää Map objektin


28.11 Arber

   Eventtracker.cc & hh:
void uppgrade_Bar(sf::Event&, std::list<Tower*>&);
	
	
	check_tower(std::list<Tower*>&, sf::Vector2i&); //Arber 25.11
	
	draw_upp_box(sf::RenderWindow&, Player &player);//Arber 25.11
	
	get_checker()const;//Arber 25.11
	
	check_left_click(sf::Event&, Player &, TowerContainer&);//Arber 25.11
	
	sell(Player &,TowerContainer&);//Arber 25.11
	
	uppgrade(Player&, TowerContainer&);//Arber 25.11

	Käytännössä nimet kertoo aika hyvin mitä ne tekee...

Tower.hh & cc:

  get_level()const; //Arber 25.11
  
  get_Price()const; //Arber 25.11
  
  get_uppgradePrice()const; // 28.11
  
  set_attributes(int,int,int);// 28.11

Muokkasin hieman myös construktorii ja uppdateTex():a. Ei pitäisi vaikuttaa muuhun koodiin mitenkään

Lisäsin näihin molempiin myös muutaman muuttujan

ja viimeseks vaihoin vielä funktion toString() paikkaa Game.hh:sta -> object.hh:hon. Sen on parempi olla siellä niin jokainen luokka pystyy sitten käyttämään sitä. :D  

Mun koodin mukana tulee jossai mukana myös pieni bugi. Kun lisätään torneja niin tulee error. Tää ei kaada peliä tai näyttänyt vaikuttavan viel mitenkää, mut korjaan sen sit
ku on taas aikaa...
	


2.12. Markus

- Lisätty peliäänet: toteutus game.cc ja .hh. warSoundsin looppaus tarkistetaan torni-iteroinnin
	yhteydessä, ja torni-iteraatiota siten modattu hieman.
- Lisätty musiikit: Tarkoitus että menussa ja pelissä soi eri musiikit loopilla, itse musiikkia
	käynnistävää koodia kuitenkaan ei vielä ole
- Tehty muutoksia projectileihin ja niiden luontiin siten, että super towerin projectile luo ison
	räjähdyksen räjähtäessään, ja projectileille on lisätty damage, range ja torneille eri
	tulinopeudet. Projectilet hakevat powerin tornilta ja vievät sen (damagen) eteenpäin
	vihollisille.
- Missilet eivät enää lennä pisteeseen 0.0 jos lentovihollisia ei ole lähistöllä, vaan jäävät
	pyörimään vihollisen vanhalle paikalle
- Spritet missileihin, maatornin MG:hen ja supertowerin pommeihin, ja niiden yksinkertainen
	animaatio

- IDEOITA:
	- Scores.txt tallentaa mapin nimen, ja näyttää mappikohtaisesti pisteet?
	- Vaihtoehtoisesti scorejen tallennus map.txt:hen, jos laitetaan vaikeustasot?
	- Inventoryyn jotain backgroundtekstuuria enemmän?
	
- TO DO:
	Torneja pystyy rakentamaan päällekäin, korjattava


6.12. Markus

- Lisätty räjähdykset kun vihollinen pääsee baseen ja iso räjähdys kun pelaaja häviää
- Lauri lisäsi reitin arpomisen jos useita mahdollisia kulkureittejä
- Korjattu tornien päällekkäin rakentaminen
- Lauri: pisteet tallennetaan map-tiedostoon ja highscoret haetaan joka mapille erikseen
- Mappeja pystyy selaamaan nuolinäppäimillä
- Torneja pystyy rakentamaan numeronäppäimillä
- Bossiviholliset lisätty
- Inventorytekstuuri ja inventoryn järjestys muokattu
- Arber tehnyt vihollisille defense-ominaisuuden joka säätelee vaikeustason mukaan kuinka paljon niitä sattuu
- Useita arvoja siirretty namespaceen

	Vielä yleistä balanssia pitäisi säätää, ja mapit voisi siirtää omaan maps kansioonsa.
	Tornien upgradeemisesta, että upgradevalikko voisi ilmestyä hiiren oikealle puolelle koska se menee ruudun
		yli, jos torni on ruudun vasemmassa laidassa


10.12. Markus

- Balanssi säädetty: Megapommin damage 50->60, vihollisten kultamääriä muutettu, nopeutta muutettu, medium-kerroin 
	muutettu 1->1.1
- Mapit omassa kansiossaan
- Tornien upgradevalikko kunnossa, lisätty tornien range
