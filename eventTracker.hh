#ifndef EVENT_TRACKER_HH
#define EVENT_TRACKER_HH

#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include <sstream>

#include "namespace.hh"
#include "objectContainer.hh"
#include "object.hh"
#include "map.hh"
#include "enemy.hh"
#include "mapInventory.hh"
#include "player.hh"
#include "effects.hh"


class EventTracker{

public:

	EventTracker();


	~EventTracker();

	// Returns false when player has ended the game
	bool checkEvents(sf::RenderWindow& window, TowerContainer& towers, Map& map, Player& player, MapInventory& inventory, Effects& effects, EnemyContainer& enemies, bool gameEnd);

	// Checks if possible to uppgrade.    Arber 25.11
	void uppgrade_Bar(sf::Event&, std::list<Tower*>&);
	//checks if user clicking a tower.    Arber 25.11
	bool check_tower(std::list<Tower*>&, sf::Vector2i&); //Arber 25.11

	void draw_upp_box(sf::RenderWindow&, Player &player);//Arber 25.11

	bool get_checker()const;//Arber 25.11

	void check_left_click(sf::Event&, Player &, TowerContainer&, Map&);//Arber 25.11

	void sell(Player &,TowerContainer&, Map&);//Arber 25.11

	void uppgrade(Player&, TowerContainer&);//Arber 25.11


private:

	EventTracker(const EventTracker& other) ;
	void operator=(const EventTracker& other) ;


	bool towermode;
	bool bombmode;
	bool bombExplosion;

	sf::Vector2i mousePos;
	int selectedTowerType;
	sf::RectangleShape rect;
	sf::Texture tex;
	
	bool gameEnded;

	Explosion bigExplosion ;

	sf::Font upp_font; //arber 25.11
	bool checker;//Arber 26.11
	int towerRange;
	int uppBoxX;//Arber 26.11
	int uppBoxY;//Arber 26.11
	int uppBoxPrice;//Arber 26.11
	int uppBoxNXT;//Arber 26.11
	int uppgradePrice;//Arber 28.11

	sf::SoundBuffer bassBombSndBuff;
	sf::Sound bassBombSound; // Ison pommin äänet tällä hetkellä tässä, onko hyvä paikka?
};





#endif
