#ifndef ENEMY_HH
#define ENEMY_HH

#include "object.hh"
#include "terrain.hh"
#include "namespace.hh"


#include <vector>
#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

enum dir { down, up, left, right }; // Kävelysuunta !! //Markus 19.11.

enum EnemyType{//Arber 21.11
  Ground,
  Fly,
  Ground_Boss,
  Fly_Boss
};

class Enemy : public Object
{
public:
	Enemy(EnemyType,int, int, float);

	/*~Enemy()
	{
		delete animationClk; //Markus 19.11.
	}*/

	void update(float, std::vector<std::vector<Terrain*>> const &);

	void updateTex();

	sf::Vector2i get_Currpos()const;

	float get_HP()const;

	void get_damage(int);

	void animate();

	void draw_HP(sf::RenderTarget&) const;

	void checkPath(sf::Vector2i, std::vector<std::vector<Terrain*>> const &);

	void updatePos(sf::Vector2i);

	void updatePosPixel(sf::Vector2f);


	int getPosPixelX() const;

	int getPosPixelY() const;

	int getPosX() const;

	int getPosY() const;

	EnemyType get_Type()const;

	int make_Damage()const;
	int get_Points()const;

	int get_Gold()const;




private:
	EnemyType type;
	int startPosX;
	int startPosY;
	float multiplier; // Difficulty multiplier
	sf::Clock* animationClk; //Markus 19.11.
	sf::Texture tex ;
	sf::Vector2i currPos ;
	sf::Vector2f currPosPixel;

	sf::RectangleShape HPBar_gre;
	sf::RectangleShape HPBar_red;
	float defense;
	int ESpeed;
	float HP;
	int Points;
	int Damage;
	int Gold;
	int anim; // Animaatio //Markus 19.11.
	dir direction; //Markus 19.11.
	dir oldDirection;
};

#endif
