#ifndef TERRAIN_HH
#define TERRAIN_HH
#include"object.hh"
#include <vector>
#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>




class Terrain : public Object
{
public:
  Terrain(int, int, int);
 
  void draw(sf::RenderTarget &) const;
 
 // void updateTex();
       
 
  int getPosX() const;
 
  int getPosY() const;
 
  int getType() const;
  
  void setType(int);
 
  //void changeTex(int type);
  
  void adjustPaths(int pathType);
 
 
private:
        int type; //GRASS = 0, PATH = 1, START=2, FINISH=3, TREE/TOWER=4
        int posX;
        int posY;
        sf::Texture terrainTex ;
       // sf::Texture mouseOver;
 
};



#endif
