#include "mapInventory.hh"

MapInventory::MapInventory()
{
  InventoryTex.loadFromFile("graphics/inventory.png");
  InventoryBox.setTexture(InventoryTex, true);
  info_font.loadFromFile("steampunk.ttf"); // 19.11. Markus
}

MapInventory::~MapInventory()
{}



void MapInventory::drawInventory(sf::RenderTarget& rt, Player& player)
{

  InventoryBox.setPosition(832, 0);
  InventoryBox.setTextureRect(sf::IntRect(192,0,64,64));
  rt.draw(InventoryBox);

  InventoryBox.setPosition(896, 0);
  InventoryBox.setTextureRect(sf::IntRect(0,64,64,64));
  rt.draw(InventoryBox);

  InventoryBox.setPosition(960, 0);
  InventoryBox.setTextureRect(sf::IntRect(128,0,64,64));
  rt.draw(InventoryBox);

  InventoryBox.setPosition(832, 64);
  InventoryBox.setTextureRect(sf::IntRect(128,64,64,64));
  rt.draw(InventoryBox);
  /*
  InventoryBox.setPosition(896, 64);
  InventoryBox.setTextureRect(sf::IntRect(64,64,64,64));
  rt.draw(InventoryBox);
  */
  InventoryBox.setPosition(960, 64);
  InventoryBox.setTextureRect(sf::IntRect(192,64,64,64));
  rt.draw(InventoryBox);

  // Draw background
  InventoryBox.setPosition(832, 192);
  InventoryBox.setTextureRect(sf::IntRect(0,400,192,576));
  rt.draw(InventoryBox);

  // Tämä piirtää laatikoita inventoryyn "täytteeksi", jos lisäät bokseja, muuta i < 6 arvoa esim. i < 5 tai i < 7)


  for(int i = 2 ; i < 3 ; i++)
  {
    InventoryBox.setPosition(832, 64 * i);
    InventoryBox.setTextureRect(sf::IntRect(0,128,64,64));
    rt.draw(InventoryBox);

    InventoryBox.setPosition(896, 64 * i);
    InventoryBox.setTextureRect(sf::IntRect(0,128,64,64));
    rt.draw(InventoryBox);

    InventoryBox.setPosition(960, 64 * i);
    InventoryBox.setTextureRect(sf::IntRect(0,128,64,64));
    rt.draw(InventoryBox);

  }


  // Piirretään ostettavat towerit


  //alue x = 832-896, y = 256 - 320, maatorni
  InventoryBox.setPosition(InvTowerX, InvGroundY);
  InventoryBox.setTextureRect(sf::IntRect(128,128,64,64));
  rt.draw(InventoryBox);


  //alue x = 896-960 y = 384 -320, ilmatorni
  InventoryBox.setPosition(InvTowerX, InvFlyingY);
  InventoryBox.setTextureRect(sf::IntRect(64,128,64,64));
  rt.draw(InventoryBox);

  //alue x = 960-1024, y = 384 -320, supertorni
  InventoryBox.setPosition(InvTowerX, InvSuperY);
  InventoryBox.setTextureRect(sf::IntRect(192,128,64,64));
  rt.draw(InventoryBox);


  //alue x = 832 - 896 ,y = 320 - 384, pommi
  InventoryBox.setPosition(InvTowerX, InvMegabombY);
  InventoryBox.setTextureRect(sf::IntRect(0,192,90,76));
  rt.draw(InventoryBox);


  // pomminappi

  //x = 860-988, y = 500-634
  InventoryBox.setPosition(InvBombButtonX, InvBombButtonY);

  if(player.getBombs() > 0)
    InventoryBox.setTextureRect(sf::IntRect(0,275,128,124)); // HUOM! Modattu 124:ään // Markus 5.12.
  else
  	InventoryBox.setTextureRect(sf::IntRect(128,257,128,134));

  rt.draw(InventoryBox);


}

// Allaoleva funktio näyttää tornin tiedot kun hiiri viedään sen ylle

void MapInventory::drawTowerInfo(sf::Vector2i mousePos, sf::RenderTarget& rt, Player &player)
{
  //std::cout << mousePos.x << "  " << mousePos.y << std::endl;


  if(mousePos.x > InvTowerX && mousePos.x < InvTowerXX && mousePos.y > InvGroundY && mousePos.y < InvGroundYY)
  {


    std::string str = "This tower shoots ground enemies\nCost: 100";


    sf::Text tower_info(str, info_font, 20);

    sf::RectangleShape infobg;
    infobg.setFillColor(sf::Color::Black);
    infobg.setSize(sf::Vector2f(300,100));
    infobg.setPosition(mousePos.x - 320,mousePos.y);
    rt.draw(infobg);
    tower_info.setPosition(mousePos.x - 310,mousePos.y);
    rt.draw(tower_info);

    if (player.getGold() < 100){
      std::string str("\nYou don't have enough money!");
      sf::Text nomoney(str, info_font, 20);
      nomoney.setColor(sf::Color::Red);
      nomoney.setPosition(mousePos.x - 310,mousePos.y + 50);
      rt.draw(nomoney);

      }

  }

  else if(mousePos.x > InvTowerX && mousePos.x < InvTowerXX && mousePos.y > InvFlyingY && mousePos.y < InvFlyingYY)
  {


    std::string str = "This tower shoots flying enemies\nCost: 200";



    sf::Text tower_info(str, info_font, 20);

    sf::RectangleShape infobg;
    infobg.setFillColor(sf::Color::Black);
    infobg.setSize(sf::Vector2f(300,100));
    infobg.setPosition(mousePos.x - 320,mousePos.y);
    rt.draw(infobg);
    tower_info.setPosition(mousePos.x - 310,mousePos.y);
    rt.draw(tower_info);

    if (player.getGold() < 200){
      std::string str("\nYou don't have enough money!");
      sf::Text nomoney(str, info_font, 20);
      nomoney.setColor(sf::Color::Red);
      nomoney.setPosition(mousePos.x - 310,mousePos.y + 50);
      rt.draw(nomoney);

     }
  }

  else if(mousePos.x > InvTowerX && mousePos.x < InvTowerXX && mousePos.y > InvSuperY && mousePos.y < InvSuperYY)
  {

    std::string str = "This supertower is awesome,\nshoots everything and has more power\nCost: 700";

    sf::Text tower_info(str, info_font, 20);

    sf::RectangleShape infobg;
    infobg.setFillColor(sf::Color::Black);
    infobg.setSize(sf::Vector2f(300,100));
    infobg.setPosition(mousePos.x - 320,mousePos.y);
    rt.draw(infobg);
    tower_info.setPosition(mousePos.x - 310,mousePos.y);
    rt.draw(tower_info);

    if (player.getGold() < 700){
      std::string str("\nYou don't have enough money!");
      sf::Text nomoney(str, info_font, 20);
      nomoney.setColor(sf::Color::Red);
      nomoney.setPosition(mousePos.x - 310,mousePos.y + 50);
      rt.draw(nomoney);

     }
  }


  else if(mousePos.x > InvTowerX && mousePos.x < InvMegabombXX && mousePos.y > InvMegabombY && mousePos.y < InvMegabombYY)
  {
    std::string str = "You can blast a bunch enemies on screen\nat once with this megabomb!!\nCost: 1000";

    sf::Text tower_info(str, info_font, 20);

    sf::RectangleShape infobg;
    infobg.setFillColor(sf::Color::Black);
    infobg.setSize(sf::Vector2f(300,100));
    infobg.setPosition(mousePos.x - 320,mousePos.y);
    rt.draw(infobg);
    tower_info.setPosition(mousePos.x - 310,mousePos.y);
    rt.draw(tower_info);

    if (player.getGold() < 1000){
      std::string str("\nYou don't have enough money!");
      sf::Text nomoney(str, info_font, 20);
      nomoney.setColor(sf::Color::Red);
      nomoney.setPosition(mousePos.x - 310,mousePos.y + 50);
      rt.draw(nomoney);

      }
  }





}


//Tässä valitaan minkä tyypin tornia on klikattu
int checkTowerSelection(sf::Vector2i& mousePos)
{
  int selection ;

  if(mousePos.x > InvTowerX && mousePos.x < InvTowerXX && mousePos.y > InvGroundY && mousePos.y < InvGroundYY)
  {
    selection = 1;
  }

  else if(mousePos.x > InvTowerX && mousePos.x < InvTowerXX && mousePos.y > InvFlyingY && mousePos.y < InvFlyingYY)
  {
    selection = 2;
  }

  else if(mousePos.x > InvTowerX && mousePos.x < InvTowerXX && mousePos.y > InvSuperY && mousePos.y < InvSuperYY)
  {
    selection = 3;
  }

  else if(mousePos.x > InvTowerX && mousePos.x < InvTowerXX && mousePos.y > InvMegabombY && mousePos.y < InvMegabombYY)
  {
  	selection = 4 ;
  }

  // Tässä tarkastetaan onko klikattu "BOMBS AWAY" nappia. Tällöin muutetaan bombmode = true
  else if(mousePos.x > InvBombButtonX && mousePos.x < InvBombButtonXX && mousePos.y > InvBombButtonY  && mousePos.y < InvBombButtonYY)
  {
  	selection = 5;
  }
  else
  {
    selection = 0 ;
  }

  return selection ;

}

int MapInventory::get_Player_Money(Player player)const{
  return player.getGold();
}



