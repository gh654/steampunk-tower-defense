#include "highscore.hh"
#include <iostream>
#include <sstream>
#include <fstream>

	Highscore::Highscore(std::string fname){
		loadScores(fname);
	}


	const std::list<std::pair<std::string,int>>& Highscore::getScores() const{
		return scores;
	}

	void Highscore::addScore(std::string name, int score){
		scores.push_back(std::pair<std::string,int>(name,score));
		sort();
	}

	void Highscore::loadScores(std::string fname){
		 std::ifstream openFile(fname);

        if(openFile.is_open()){
			bool read = false;
            while(!openFile.eof()){
                std::string str, name, score;
                std::getline(openFile, str);


				if(str.find("-HighscoresEnd") != std::string::npos)
					read = false;

				std::stringstream ss(str);

				if(read && !str.empty()){
					std::getline(ss, name, ' ');
					std::getline(ss, score, ' ');

					scores.push_back(std::pair<std::string,int>(name,std::strtol(score.c_str(), NULL, 10)));
				}
				if(str.find("-HighscoresBegin") != std::string::npos)
					read = true;
			}

		}
}
	//MUOKATTU 6.12. -Lauri
	void Highscore::saveScores(std::string fname){
		std::ifstream openFile(fname);
		
		if(openFile.is_open()){
			std::stringstream ss;
			
			while(!openFile.eof()){
                std::string str;
                std::getline(openFile, str);
				ss << str << std::endl;
				if(str.find("-HighscoresBegin") != std::string::npos){
					break;
				}
			}

			openFile.close();
			
			for(auto iterS = scores.begin(); iterS != scores.end(); iterS++){
				ss << iterS->first << " " << iterS->second << std::endl;
			}
			
			ss << "-HighscoresEnd" << std::endl;
			std::ofstream outputFile(fname);
			outputFile << ss.str();
		}

	}


	bool compare_scores(std::pair<std::string,int> first, std::pair<std::string,int> second){
		return first.second > second.second;
	}


	void Highscore::sort(){
		scores.sort(compare_scores);
	}
	
	//UUSI 6.12. -Lauri
	void Highscore::clearScores(){
		scores.clear();
	}


	std::ostream& operator<<(std::ostream& os, const Highscore& hscore){

		auto hscores = hscore.getScores();
		int counter = 1;
        for(auto iter = hscores.begin(); iter != hscores.end(); iter++){
                os << counter << ". " << iter->first << ": " << iter->second << std::endl;
				counter++;
		}

        return os;
    }
