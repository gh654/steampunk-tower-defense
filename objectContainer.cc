
#include"objectContainer.hh"



	 EnemyContainer::EnemyContainer()
	 {}

	 EnemyContainer::~EnemyContainer()
	 {
	   for(auto iter = enemyList.begin(); iter != enemyList.end(); iter++)
	   {
	     delete *iter;
	   }
	   enemyList.clear();
	 }

	 std::list<Enemy*>& EnemyContainer::getEnemies()
	 {
	   return enemyList ;
	 }


	 void EnemyContainer::addEnemy(Enemy* new_enemy)
	 {
	   enemyList.push_back(new_enemy) ;
	 }


	 void EnemyContainer::deleteEnemy(std::list<Enemy*>::iterator iter)
	 {
	   Enemy* temp = *(iter);
	   enemyList.erase(iter);
	   delete temp;

	 }

	 void EnemyContainer::updateLast()
	 {
	   enemyList.back()->updateTex();
	 }



	 bool EnemyContainer::isEmpty()
	 {
	   return enemyList.empty();
	 }

	 void EnemyContainer::makeEnemies(EnemyType type, sf::Vector2i start, float multiplier) // start vectori lisätty 25.11 -Lauri
	 {
	   addEnemy(new Enemy(type,start.x, start.y, multiplier));
	   updateLast();
	 }

	 /*
	 void EnemyContainer::updateWave(EnemyType type, Map &map) // ??
	 {
	   sf::Vector2i startPoint = map.getStart();
	   addEnemy(new Enemy(type, startPoint.x,startPoint.y));
	   updateLast();
	 }*/

	void EnemyContainer::check_HP(std::list<Enemy*>::iterator enem, Player& player){
	 if((*enem)->get_HP() == 0){

	   player.addPoints((*enem)->get_Points());
	   player.addGold((*enem)->get_Gold());
	   deleteEnemy(enem);
	   }
	 }

	 void EnemyContainer::checkAllHP(Player& player){


	 	for(auto iter = enemyList.begin(); iter != enemyList.end();)
	 	{
	 		if((*iter)->get_HP() == 0)
	 		{
	 			player.addPoints((*iter)->get_Points());
	 			player.addGold((*iter)->get_Gold());
	  			iter = enemyList.erase(iter++);
	 		}
	 		else
	 			++iter ;
	 	}
	 }

	 std::list<Enemy*>::iterator EnemyContainer::check_Finnish( std::list<Enemy*>::iterator enem, std::vector<std::vector<Terrain*>> const &coll_map, Player& player){


	   int TileY = (*enem)->get_Currpos().x;
	   int TileX = (*enem)->get_Currpos().y;

	   auto x = enem;
	   enem++;

	   if(coll_map[TileX][TileY]->getType()  == 3) // coll_map[TileY][TileX]->getPosX() == (*enem)->getPosX() && coll_map[TileY][TileX]->getPosY() == (*enem)->getPosY())
	   {


	     player.reduceHP((*x)->make_Damage()); // Korjattu 25.11. -Lauri
	     deleteEnemy(x);
	   }
	 return enem;
}

 //UUSI 28.11. -Lauri
void EnemyContainer::clearContainer(){
   for(auto iter = enemyList.begin(); iter != enemyList.end(); iter++)
   {
	 delete *iter;
   }
   enemyList.clear();
}


	 //Tower:

	 TowerContainer::TowerContainer()
	 {}

	TowerContainer::~TowerContainer()
	{
	  for(auto iter = towerList.begin(); iter != towerList.end(); iter++)
	  {
	  	delete *iter;
	  }
	  	towerList.clear();
	}

	std::list<Tower*>& TowerContainer::getTowers()
	{
		return towerList ;
	}


	void TowerContainer::addTower(Tower* new_tower)
	{
		towerList.push_back(new_tower) ;
	}

	void TowerContainer::deleteTower(std::list<Tower*>::iterator iter)
	{
		Tower* temp = *(iter);
		towerList.erase(iter);
		delete temp;
	}

	void TowerContainer::updateLast(int type)
	{
		towerList.back()->updateTex(type);
	}

	bool TowerContainer::isEmpty()
	{
		return towerList.empty();
	}


	void TowerContainer::addTowers(int x, int y, int type)
	{
	  int xTile;
	  int yTile;

	  xTile = x / 64 ;
	  yTile = y / 64 ;

	  addTower(new Tower(type, xTile, yTile)) ;
	  updateLast(type);
	}

 //UUSI 28.11. -Lauri
void TowerContainer::clearContainer()
{
	  for(auto iter = towerList.begin(); iter != towerList.end(); iter++)
	  {
		delete *iter;
	  }
	towerList.clear();
}
	//Projectile


	ProjectileContainer::ProjectileContainer()
	  {}

	  ProjectileContainer::~ProjectileContainer()
	  {
	  	for(auto iter = projList.begin(); iter != projList.end(); iter++)
	  	{
	  		delete *iter;
	  	}
	  	projList.clear();
	  }

	  std::list<Projectile*>& ProjectileContainer::getProjectiles()
	  {
		return projList ;
	  }


	 void ProjectileContainer::addProjectile(Projectile* new_proj)
	 {
		projList.push_back(new_proj) ;
	 }

	 void ProjectileContainer::updateProjectiles()
	 {
		for(auto iter = projList.begin(); iter != projList.end(); iter++)
		{
				if ((*iter)->isPast())
				{
					//std::cout << "Poistetaan projectile" << std::endl ; //debuggaamiseen
					delete *iter; // Lisätty 13.11. - Lauri
					iter = projList.erase(iter);
					iter--;
					//projectiles.deleteProjectile();
				}
		}
	 }

	 void ProjectileContainer::updateLast()
	 {
	 	projList.back()->updateTex();
	 }

	bool ProjectileContainer::isEmpty()
	{
		return projList.empty();
	}

 //UUSI 28.11. -Lauri
void ProjectileContainer::clearContainer()
{
	for(auto iter = projList.begin(); iter != projList.end(); iter++)
	{
		delete *iter;
	}
	projList.clear();
}






