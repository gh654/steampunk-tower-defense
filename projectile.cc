#include "projectile.hh"



Projectile::Projectile(sf::Vector2f enemypos, sf::Vector2f pos, int type, int power) : Object(20, 20), posX(pos.x), posY(pos.y), target(enemypos), type(type), aliveTime(new sf::Clock), Power(power)
    {
        updateTex();
        rect.setPosition(posX, posY); // Aseta keskelle tornitileä
        sequence = 0;
		//target = enemy.getCenter();

		float vx = (target.x - posX);
		float vy = (target.y - posY);
		float length = sqrt(pow(target.x - posX, 2) + pow(target.y - posY, 2));
		rect.rotate(180 / PI * atan(vy/vx));
		if (vx >= 0) rect.rotate(180);

		/*
		if (type == 1)
		{
			vx *= 10;
			vy *= 10;
		}
		*/
		vx /= length;
		vy /= length;
		if (type == 1)
        {
            vx *= 800;
            vy *= 800;
        }
		if (type == 2)
        {
            vx *= 500;
            vy *= 500;
        }
        if (type == 3)
        {
            vx *= 200;
            vy *= 200;
        }
        //std::cout << "Projectile vx " << vx << " vy " << vy << " Rotation " << rect.getRotation() << std::endl;

		v = sf::Vector2f(vx ,vy);

		pastTarget = false;
		if (type == 2) range = AirRange; // Missilen range /Markus 25.11.
		else range = AreaBombRange; // Areapommin range
    }


void Projectile::update(float dt, sf::Vector2f targetCenter)
    {

		if (type == 1 || type == 3) // Animate
        {
            if (sequence < 1)
            {
                rect.setTextureRect(sf::IntRect(0, 0, 64, 64));
                sequence++;
            }
            else
            {
                rect.setTextureRect(sf::IntRect(64, 0, 64, 64));
                sequence = 0;
                //sequence++;
                //if (sequence > 2) sequence = 0;
            }
        }

		if (type == 2) // Missile
		{
			if (targetCenter != sf::Vector2f(0,0))
				target = targetCenter;

			float vx = (target.x - rect.getPosition().x);
			float vy = (target.y - rect.getPosition().y);
			float length = sqrt(pow(target.x - rect.getPosition().x, 2) + pow(target.y - rect.getPosition().y, 2));
			vx /= length;
			vy /= length;
			vx *= 200;
			vy *= 200;
			rect.rotate(180 / PI * atan(vy/vx));
			v = sf::Vector2f(vx ,vy);

			if (aliveTime->getElapsedTime().asSeconds() > 1.5) // Range drops after 1.5 sec
                range = 150;
		}

		rect.move(dt * v);

		if (type != 2)
		{

		if ((posX - target.x) > 0) // Jos lähdetään oikealta
		{
			if ((posY - target.y) > 0) { // Jos lähdetään alhaalta targetiin nähden
				if (rect.getPosition().x <= target.x && rect.getPosition().y <= target.y) { // jos ollaan ylävasemmalla
					pastTarget = true;
				}
			}

			else // Lähdetään ylhäältä tai tasasta
				if (rect.getPosition().x <= target.x && rect.getPosition().y >= target.y) { // jos ollaan alavasemmalla
					pastTarget = true;
				}
		}
		else // Lähdetään vasemmalta tai tasasta
		{
			if ((posY - target.y) > 0) { // Jos lähdetään alhaalta targetiin nähden
				if (rect.getPosition().x >= target.x && rect.getPosition().y <= target.y) { // jos ollaan yläoikealla
					pastTarget = true;
				}
			}

			else // Lähdetään ylhäältä tai tasasta
				if (rect.getPosition().x >= target.x && rect.getPosition().y >= target.y) { // jos ollaan alaoikealla
					pastTarget = true;

				}
		}
	}

    }

bool Projectile::isPast() // Onko projectile ohittanut kohteensa
{
	return pastTarget;
}

void Projectile::setPast()
{
	if (pastTarget == false)
		pastTarget = true;
	else
		pastTarget = false;
}


void Projectile::updateTex()
    {
		if (get_type() == 1)
        {
            tex.loadFromFile("graphics/MGprojectile.png");
        }

        if (get_type() == 2)
        {
            tex.loadFromFile("graphics/missileProjectileBlue.png");
        }

        if (get_type() == 3)
        {
            tex.loadFromFile("graphics/supertowerbomb.png");
        }

        rect.setTexture(&tex, true);
        rect.setTextureRect(sf::IntRect(0, 0, 64, 64));
    }


int Projectile::get_type()const
	{
		return type;
	}


bool Projectile::check_Enemy(Enemy enemy) // Kopsattu towerista
	{
		switch (get_type()){
		case 1:
		if (enemy.get_Type() == Ground || enemy.get_Type() == Ground_Boss)
		return true;
		else
		return false;
		break;
		case 2:
		if (enemy.get_Type() == Fly || enemy.get_Type() == Fly_Boss)
		return true;
		else
		return false;
		break;
		default:
		return true;
		}
	}


int Projectile::getRange()
	{
		return range;
	}

int Projectile::make_Damage()const{
  return Power;

}

bool Projectile::missileTimeout()
{
	if (type == 2 && aliveTime->getElapsedTime().asSeconds() > 3)
	{
		return true;
	}
	return false;
}

sf::Vector2f Projectile::getTarget() const
{
	return target;
}
