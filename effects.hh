#ifndef EFFECTS_HH
#define EFFECTS_HH

#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

enum blowdir{ blow_up, blow_right, blow_down, blow_left };


class Effects
{

public:

	Effects();

	~Effects();

	sf::Texture explosionTex;

private:
	Effects(const Effects& other) ;
	void operator=(const Effects& other) ;

};


class Explosion
{
public:

	Explosion(int, int, int = 0);

	Explosion();

	~Explosion();

	void animate(sf::Texture& tex, sf::RenderTarget& rt);

	bool animateBig(sf::Texture& tex, sf::RenderTarget& rt);

	int getSequenceX() const;
	int getSequenceY() const;
	int getType() const;

	void giveCoordinates(sf::Vector2i& coords) ;
	void setSequence(int,int);
	void resetCounter() ;

private:
	Explosion(const Explosion& other) ;
	void operator=(const Explosion& other) ;

	int posX;
	int posY;
	int type; // 0 = MG, 1 = missile, 2 = big, 3 = huge
	int sequenceX;
	int sequenceY;
	int blowCounter ;
	sf::RectangleShape rect ;
	blowdir direction ;

};




#endif
